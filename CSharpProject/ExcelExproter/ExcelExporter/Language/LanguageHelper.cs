﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using OfficeOpenXml;
using Newtonsoft.Json;

namespace XFramework
{
    public static class LanguageHelper
    {
        public const string FileName = "多语言.xlsx";

        private const string LanguageSheetName = "GenLanguage";

        static Dictionary<string, string> languageWithKey = new Dictionary<string, string>();

        static Dictionary<string, string> keyWithLanguage = new Dictionary<string, string>();

        /// <summary>
        /// key的列号
        /// </summary>
        static int keyCol = 0;

        /// <summary>
        /// cn的列号
        /// </summary>
        static int cnCol = 0;

        /// <summary>
        /// 当前行数
        /// </summary>
        static int rowNum = 0;

        /// <summary>
        /// 插入新行的起点
        /// </summary>
        static int insertRow = 0;

        public static bool TryGetValueByKey(string key, out string value)
        {
            return keyWithLanguage.TryGetValue(key, out value);
        }

        public static bool TryGetKeyByValue(string value, out string key)
        {
            return languageWithKey.TryGetValue(value, out key);
        }

        /// <summary>
        /// 准备开始
        /// </summary>
        public static void Prepare()
        {
            using var stream = File.OpenRead(Path.Combine(Program.GetExcelPath(), FileName));
            using ExcelPackage ep = new ExcelPackage(stream);

            // 找到Key所在的列和CN所在的列
            foreach (ExcelWorksheet sheet in ep.Workbook.Worksheets)
            {
                if (sheet.Name != LanguageSheetName)
                    continue;

                const int row = 4;
                const string key = "key";
                const string cn = "cn";

                for (int col = 1; col <= sheet.Dimension.End.Column; col++)
                {
                    var cellText = sheet.Cells[row, col].Text.Trim();
                    if (cellText.ToLower() == key)
                        keyCol = col;
                    else if (cellText.ToLower() == cn)
                        cnCol = col;
                    else if (cellText == Program.EndFlag)
                        break;
                    
                    if (keyCol > 0 && cnCol > 0)
                        break;
                }

                if (keyCol > 0 && cnCol > 0)
                    break;
            }

            if (keyCol * cnCol == 0)
                return;

            // 将表里的Key和CN的值对应存起来
            foreach (ExcelWorksheet sheet in ep.Workbook.Worksheets)
            {
                if (sheet.Name != LanguageSheetName)
                    continue;

                rowNum = 0;
                insertRow = sheet.Dimension.End.Row + 1;
                for (int row = 6; row <= sheet.Dimension.End.Row; row++)
                {
                    string flag = sheet.Cells[row, 1].Text.Trim();
                    if (flag == Program.EndFlag)
                    {
                        insertRow = row;
                        break;
                    }

                    ++rowNum;
                    string key = sheet.Cells[row, keyCol].Text.Trim();
                    string cn = sheet.Cells[row, cnCol].Text.Trim();

                    if (string.IsNullOrEmpty(key) || string.IsNullOrEmpty(cn))
                        continue;

                    keyWithLanguage.Add(key, cn);
                    languageWithKey.Add(cn, key);
                }
            }
        }

        /// <summary>
        /// 写入
        /// </summary>
        /// <param name="keyValues"></param>
        public static void Write(ICollection<KeyValuePair<string, string>> keyValues)
        {
            if (keyValues.Count == 0)
                return;

            if (keyCol * cnCol == 0)
                return;

            byte[] bytes = null;
            string filePath = Path.Combine(Program.GetExcelPath(), FileName);
            using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using ExcelPackage ep = new ExcelPackage(stream);
                foreach (ExcelWorksheet sheet in ep.Workbook.Worksheets)
                {
                    if (sheet.Name != LanguageSheetName)
                        continue;

                    int row = insertRow;
                    sheet.InsertRow(row, keyValues.Count);  // 插入新行
                    foreach (var item in keyValues)
                    {
                        string key = item.Key;
                        string cn = item.Value;
                        sheet.Cells[row, 1].Value = ++rowNum;
                        sheet.Cells[row, keyCol].Value = key;
                        sheet.Cells[row, cnCol].Value = cn;
                        ++row;
                    }
                }
                bytes = ep.GetAsByteArray();
            }

            if (bytes != null)
            {
                File.WriteAllBytes(filePath, bytes);
            }            
        }
    }
}
