﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace XFramework
{
    public static class PathConfig
    {
        public const string ConfigPath = "path.config";

        /// <summary>
        /// 存放路径
        /// </summary>
        private static Dictionary<string, string> pathDict = new Dictionary<string, string>();

        /// <summary>
        /// 配置模板
        /// </summary>
        private static string[] pathTemplate =
        {
            "//Excel读取路径(如果不填则使用默认路径)",
            $"//默认路径 -> {Program.ExcelPath}",
            $"{nameof(Program.ExcelPath)}=\"\"",
            "",
            "//Unity里存放配置资源路径(如果不填则使用默认路径)",
            $"//默认路径 -> {Program.OutputConfigPath}",
            $"{nameof(Program.OutputConfigPath)}=\"\"",
            "",
            "//Unity里存放配置类的路径(如果不填则使用默认路径)",
            $"//默认路径 -> {Program.UnityOutputClassPath}",
            $"{nameof(Program.UnityOutputClassPath)}=\"\"",
        };

        static PathConfig()
        {
            if (File.Exists(ConfigPath))
            {
                var lines = File.ReadAllLines(ConfigPath);
                foreach (var line in lines)
                {
                    if (string.IsNullOrEmpty(line))
                        continue;

                    if (line.Trim().StartsWith("//"))
                        continue;

                    string[] values = line.Split("=");
                    if (values.Length != 2)
                        continue;

                    bool skip = false;
                    for (int i = 0; i < values.Length; i++)
                    {
                        string str = values[i].Trim().Replace("\"", "").Replace("\'", "");
                        if (string.IsNullOrEmpty(str))
                        {
                            skip = true;
                            break;
                        }

                        values[i] = str;
                    }

                    if (skip)
                        continue;

                    string key = values[0].Trim().ToLower();
                    string value = values[1].Trim();
                    pathDict.Add(key, value);
                }
            }
        }

        /// <summary>
        /// 尝试写入配置路径模板
        /// </summary>
        public static void TryWritePathTemplate()
        {
            if (!File.Exists(ConfigPath))
            {
                File.WriteAllLines(ConfigPath, pathTemplate);
                Console.WriteLine($"生成路径配置模板成功 -> {Path.GetFullPath(ConfigPath)}");
            }
            else
            {
                Console.WriteLine($"路径配置模板已存在 -> {Path.GetFullPath(ConfigPath)}");
            }
        }

        public static bool TryGetPath(string key, out string path)
        {
            return pathDict.TryGetValue(key, out path);
        }
    }
}
