﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using OfficeOpenXml;
using Newtonsoft.Json;

namespace XFramework
{
    public struct SheetInfo
    {
        /// <summary>
        /// 概括
        /// </summary>
        public string summary;
        /// <summary>
        /// 属性类型
        /// </summary>
        public string property;
        /// <summary>
        /// 字段名
        /// </summary>
        public string fieldName;

        public SheetInfo(string summary, string property, string fieldName)
        {
            this.summary = summary;
            this.property = property;
            this.fieldName = fieldName;
        }
    }

    class Program
    {
        /// <summary>
        /// 根目录
        /// </summary>
        public const string RootPath = "../../../";
        /// <summary>
        /// 读取Excel的路径
        /// </summary>
        public const string ExcelPath = RootPath + "Excel";
        /// <summary>
        /// Unity里存放资源路径
        /// </summary>
        public const string OutputConfigPath = RootPath + "UnityProject/Assets/Res/Config/Gen";
        /// <summary>
        /// Unity里存放配置类的路径
        /// </summary>
        public const string UnityOutputClassPath = RootPath + "UnityProject/Assets/Scripts/XFDemo/Config/Gen";
        /// <summary>
        /// 此项目存放配置类的路径
        /// </summary>
        public const string ThisOutputClassPath = "../ExcelExporter/Config";
        /// <summary>
        /// 存放Json路径
        /// </summary>
        public const string JsonPath ="../Json";
        /// <summary>
        /// 存放差异md5的路径
        /// </summary>
        public const string Md5Path = "../Md5";
        /// <summary>
        /// Excel的md5文件
        /// </summary>
        public const string Md5FilePath = "../MD5/ExcelMd5.txt";
        /// <summary>
        /// 需要导出的Excel(与保存的有差异)
        /// </summary>
        public const string DiffFilePath = "../MD5/ExportExcel.txt";

        /// <summary>
        /// 代码模板路径
        /// </summary>
        public const string CodeTemplateFilePath = "../ExcelExporter/Template/ConfigTemplate1.txt";

        /// <summary>
        /// 数组的分隔符
        /// </summary>
        public const string Delimiter = ";";

        /// <summary>
        /// 标记的多语言前缀
        /// </summary>
        public const string LanguagePrefix = "$";

        /// <summary>
        /// 结束的标记
        /// </summary>
        public const string EndFlag = "__END";

        private static string file = "";

        /// <summary>
        /// excel的md5
        /// </summary>
        static Dictionary<string, string> fileMd5Dict = new Dictionary<string, string>();

        /// <summary>
        /// 有差异的excel工作表
        /// </summary>
        static HashSet<string> diffConfigList = new HashSet<string>();

        /// <summary>
        /// 转换的多语言key value
        /// </summary>
        static SortedDictionary<string, string> languageKeyValue = new SortedDictionary<string, string>();

        /// <summary>
        /// 转换的多语言value key
        /// </summary>
        static Dictionary<string, string> languageValueKey = new Dictionary<string, string>();

        static void Main(string[] args)
        {
            if (!Directory.Exists(Md5Path))
                Directory.CreateDirectory(Md5Path);

            if (args.Length > 0)
            {
                string arg = args[0];
                if (arg == "exportAll")
                {
                    if (File.Exists(Md5FilePath))
                        File.Delete(Md5FilePath);

                    if (File.Exists(DiffFilePath))
                        File.Delete(DiffFilePath);

                    Run();
                }                    
                else if (arg == "export")
                {
                    Run();
                }
                else if (arg == "serialize")
                {
                    ExportExcelProtobuf();
                }
                else if (arg == "pathConfig")
                {
                    PathConfig.TryWritePathTemplate();
                }
            }
            //Console.Read();
        }

        static string GetPath(string name, string defaultPath)
        {
            if (PathConfig.TryGetPath(name.ToLower(), out var path))
                return path;

            return defaultPath;
        }

        /// <summary>
        /// 读取Excel的路径
        /// </summary>
        /// <returns></returns>
        public static string GetExcelPath()
        {
            return GetPath(nameof(ExcelPath), ExcelPath);
        }

        /// <summary>
        /// Unity里存放资源路径
        /// </summary>
        /// <returns></returns>
        static string GetOutputConfigPath()
        {
            return GetPath(nameof(OutputConfigPath), OutputConfigPath);
        }

        /// <summary>
        /// Unity里存放配置类的路径
        /// </summary>
        /// <returns></returns>
        static string GetUnityOutputClassPath()
        {
            return GetPath(nameof(UnityOutputClassPath), UnityOutputClassPath);
        }

        static void Run()
        {
            try
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                string excelPath = GetExcelPath();
                if (!Directory.Exists(excelPath))
                {
                    throw new Exception($"Excel目录不存在 -> {excelPath}");
                }

                if (File.Exists(Md5FilePath))
                {
                    fileMd5Dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText(Md5FilePath));
                }

                LanguageHelper.Prepare();
                List<string> filesPath = new List<string>(Directory.GetFiles(excelPath));

                int languageFileIndex = filesPath.FindIndex((path) => Path.GetFileName(path) == LanguageHelper.FileName);
                if (languageFileIndex >= 0)
                {
                    // 将多语言表放到最后操作
                    string path = filesPath[languageFileIndex];
                    filesPath.RemoveAt(languageFileIndex);
                    filesPath.Add(path);
                }

                for (int i = 0; i < filesPath.Count; i++)
                {
                    string path = filesPath[i];
                    string fileName = Path.GetFileName(path);
                    if (!fileName.EndsWith(".xlsx") || fileName.StartsWith("~$"))
                        continue;

                    if (i == filesPath.Count - 1)
                    {
                        // 多语言表
                        if (languageKeyValue.Count > 0)
                        {
                            LanguageHelper.Write(languageKeyValue);
                        }
                    }

                    // 检验md5，md5相同的话说明没有变更，就不用重复导出
                    var md5 = GetFileMd5(path);
                    if (fileMd5Dict.TryGetValue(fileName, out var fileMd5))
                    {
                        if (md5 == fileMd5)
                            continue;
                    }

                    fileMd5Dict[fileName] = md5;

                    using Stream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    using ExcelPackage ep = new ExcelPackage(stream);
                    string name = Path.GetFileNameWithoutExtension(path);
                    //Console.WriteLine($"fileName = {fileName}, name = {name}");

                    ExportBySheet(ep, name);
                }

                file = "";

                string dictJson = JsonConvert.SerializeObject(fileMd5Dict, Formatting.Indented);
                string diffJson = JsonConvert.SerializeObject(diffConfigList, Formatting.Indented);

                File.WriteAllText(Md5FilePath, dictJson);
                File.WriteAllText(DiffFilePath, diffJson);
            }
            catch (Exception e)
            {
                Console.WriteLine($"{file}\n" + e);
                for (int i = 0; i < 100000; i++)
                {
                    Console.Read();
                }
            }
        }

        /// <summary>
        /// 获取文件的md5
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        static string GetFileMd5(string filePath)
        {
            using Stream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            return ComputeMd5Hash(stream);
        }

        /// <summary>
        /// 获取文件的md5
        /// </summary>
        /// <param name="steam"></param>
        /// <returns></returns>
        static string ComputeMd5Hash(Stream steam)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] bytes = md5.ComputeHash(steam);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                sb.Append(bytes[i].ToString("x2"));
            }

            return sb.ToString();
        }

        /// <summary>
        /// 通过工作表导出成类
        /// </summary>
        /// <param name="ep"></param>
        /// <param name="name"></param>
        static void ExportBySheet(ExcelPackage ep, string name)
        {
            HashSet<string> fieldType = new HashSet<string>();
            List<SheetInfo> sheetInfos = new List<SheetInfo>();
            const string FieldPattern1 = @"^[_A-Za-z]";
            const string FieldPattern2 = @"^[_A-Za-z0-9]+$";
            const string Id = "Id";

            const int row = 2;
            foreach (ExcelWorksheet sheet in ep.Workbook.Worksheets)
            {
                fieldType.Clear();
                sheetInfos.Clear();

                string sheetName = sheet.Name.Trim();
                if (sheetName.StartsWith("#") || sheetName.Contains("Sheet"))
                    continue;

                file = name + "/" + sheetName;
                for (int col = 1; col <= sheet.Dimension.End.Column; col++)
                {
                    string header = sheet.Cells[row, col].Text.Trim();
                    if (header.StartsWith("#"))
                        continue;

                    string fieldProperty = sheet.Cells[row + 3, col].Text.Trim();
                    if (fieldProperty == EndFlag)
                        break;

                    string fieldName = sheet.Cells[row + 2, col].Text.Trim();
                    if (!fieldType.Add(fieldName))
                    {
                        throw new Exception($"重复的字段名{fieldName}");
                    }

                    if (!Regex.IsMatch(fieldName, FieldPattern1) || !Regex.IsMatch(fieldName, FieldPattern2))
                    {
                        throw new Exception($"不支持的字段名{fieldName}, cell[{row + 2}, {col}]");
                    }

                    string summary = sheet.Cells[row + 1, col].Text;

                    SheetInfo sheetInfo = new SheetInfo(summary, fieldProperty, fieldName);
                    sheetInfos.Add(sheetInfo);
                }

                if (!fieldType.Contains(Id))
                {
                    throw new Exception($"{name}文件->{sheetName}表没有包含字段{Id}");
                }

                string configName = FormatSheetName(sheetName);
                diffConfigList.Add(configName);
                ExportJson(sheet, sheetInfos, configName);
                ExportClass(sheetInfos, configName);
            }
        }

        /// <summary>
        /// 格式化表名
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        static string FormatSheetName(string name)
        {
            if (name.EndsWith("config", true, null))
                name = name.Substring(0, name.Length - 6);

            name += "Config";
            return name;
        }

        /// <summary>
        /// 导出成Class
        /// </summary>
        /// <param name="sheetInfos"></param>
        /// <param name="className"></param>
        static void ExportClass(List<SheetInfo> sheetInfos, string className)
        {
            if (!File.Exists(CodeTemplateFilePath))
            {
                throw new Exception($"代码模板文件不存在, 检查路径 {CodeTemplateFilePath}");
            }

            var code = File.ReadAllText(CodeTemplateFilePath);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < sheetInfos.Count; i++)
            {
                int count = i + 1;
                var info = sheetInfos[i];
                sb.AppendLine($"\t\t/// <summary>");

                string[] summaries = info.summary.Split('\n');
                for (int j = 0; j < summaries.Length; j++)
                {
                    string str = summaries[j];
                    if (j == 0)
                        sb.AppendLine($"\t\t/// {str}");
                    else
                        sb.AppendLine($"\t\t/// <para>{str}</para>");
                }
                sb.AppendLine($"\t\t/// </summary>");
                sb.AppendLine($"\t\t[ProtoMember({count}, IsRequired = true)]");
                sb.AppendLine($"\t\t[JsonProperty]");
                sb.AppendLine($"\t\tpublic {FormatProperty(info.property)} {info.fieldName} {{ get; private set; }}");
            }

            var thisCode = code.Replace("(className)", className).Replace("(CONTENT)", sb.ToString()).Replace("(BELONGTOFILE)", file);
            //var unityCode = thisCode.Replace("//(CONTENT1)", "\t\t\tlist.Clear();").Replace("//(INITCONFIG)", FormatInitArray(sheetInfos));
            var unityCode = thisCode.Replace("//(INITCONFIG)", FormatInitArray(sheetInfos));

            var outputClassPath = GetUnityOutputClassPath();
            if (!Directory.Exists(outputClassPath))
                Directory.CreateDirectory(outputClassPath);

            if (!Directory.Exists(ThisOutputClassPath))
                Directory.CreateDirectory(ThisOutputClassPath);

            string filePath = $"{outputClassPath}/{className}.cs";
            File.WriteAllText(filePath, unityCode);
            filePath = $"{ThisOutputClassPath}/{className}.cs";
            File.WriteAllText(filePath, thisCode);
        }

        /// <summary>
        /// 导出成Json
        /// </summary>
        /// <param name="sheetInfos"></param>
        /// <param name="jsonName"></param>
        static void ExportJson(ExcelWorksheet sheet, List<SheetInfo> sheetInfos, string jsonName)
        {
            StringBuilder sb = new StringBuilder();
            bool end = false;

            sb.AppendLine("{\"list\":[");
            for (int row = 6; row <= sheet.Dimension.End.Row; row++)
            {
                string id = row.ToString();
                //for (int col = 1; col <= sheet.Dimension.End.Column; col++)
                for (int col = 1; col <= sheetInfos.Count; col++)
                {
                    var info = sheetInfos[col - 1];
                    string value = sheet.Cells[row, col].Text.Trim();
                    if (col == 1)
                    {
                        if (value == EndFlag)
                        {
                            end = true;
                            break;
                        }
                        
                        sb.Append("{");
                    }

                    value = value.Replace("\"", "\\\"").Replace("\n", "\\n");
                    if (info.fieldName == "Id")
                        id = value;

                    string content = $"\"{info.fieldName}\":{GetPropertyValue(jsonName, info.fieldName, info.property, value, id)}";
                    if (col == 1)
                        sb.Append(content);
                    else
                        sb.Append($",{content}");
                }
                if (end)
                    break;

                sb.AppendLine("},");
            }
            sb.AppendLine("]}");

            if (!Directory.Exists(JsonPath))
                Directory.CreateDirectory(JsonPath);

            string filePath = $"{JsonPath}/{jsonName}.txt";
            File.WriteAllText(filePath, sb.ToString());
        }

        /// <summary>
        /// 若包含多语言前缀，则移除掉
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        static bool FormatProperty(ref string property)
        {
            if (property.StartsWith(LanguagePrefix))
            {
                property = property.Substring(1);
                return true;
            }

            return false;
        }

        /// <summary>
        /// 若包含多语言前缀，则移除掉
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        static string FormatProperty(string property)
        {
            if (property.StartsWith(LanguagePrefix))
            {
                return property.Substring(1);
            }

            return property;
        }

        /// <summary>
        /// 通过数据类型返回json的值
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="property"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        static string GetPropertyValue(string configName, string fieldName, string property, string value, string id)
        {
            string copyProperty = FormatProperty(property);
            switch (copyProperty)
            {
                case "bool":
                    return FormatBoolValue(fieldName, property, value);
                case "int":
                    return FormatNormalValue(fieldName, property, value, s => int.TryParse(s, out var v));
                case "long":
                    return FormatNormalValue(fieldName, property, value, s => long.TryParse(s, out var v));
                case "float":
                    return FormatNormalValue(fieldName, property, value, s => float.TryParse(s, out var v));
                case "double":
                    return FormatNormalValue(fieldName, property, value, s => double.TryParse(s, out var v));
                case "bool[]":
                    return FormatBoolArray(fieldName, property, value);
                case "float[]":
                    return FormatNormalArray(fieldName, property, value, s => float.TryParse(s, out var v));
                case "double[]":
                    return FormatNormalArray(fieldName, property, value, s => double.TryParse(s, out var v));
                case "int[]":
                    return FormatNormalArray(fieldName, property, value, s => int.TryParse(s, out var v));
                case "long[]":
                    return FormatNormalArray(fieldName, property, value, s => long.TryParse(s, out var v));
                case "string":
                    return FormatLanguageKey(configName, id, fieldName, property, value);
                case "string[]":
                    return FormatStringArray(configName, id, fieldName, property, value);
                default:
                    throw new Exception($"无法识别的类型：{property}");
            }
        }

        /// <summary>
        /// 值转换抛异常
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="property"></param>
        /// <param name="invalidStr"></param>
        /// <exception cref="Exception"></exception>
        static void ThrowPropertyValue(string fieldName, string property, string invalidStr)
        {
            throw new Exception($"{fieldName} -> {property}不支持的值{invalidStr}");
        }

        /// <summary>
        /// 格式化bool值
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="property"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        static string FormatBoolValue(string fieldName, string property, string value)
        {
            if (string.IsNullOrEmpty(value) || value == "0")
                return "false";

            string str = value.ToLower();
            if (str == "false" || str == "true")
                return str;

            if (value == "1")
                return "true";

            ThrowPropertyValue(fieldName, property, value);

            return value;
        }

        /// <summary>
        /// 格式化bool数组
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="property"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        static string FormatBoolArray(string fieldName, string property, string value)
        {
            if (string.IsNullOrEmpty(value))
                return "null";

            string str = value.Replace(Delimiter, ",");
            if (str.EndsWith(","))
                str += "false";

            string[] values = str.Split(',');
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < values.Length; i++)
            {
                string v = values[i];
                string ret = FormatBoolValue(fieldName, property, v);
                if (i == 0)
                    sb.Append(ret);
                else
                    sb.Append($",{ret}");
            }

            str = "[" + sb.ToString() + "]";
            return str;
        }

        /// <summary>
        /// 格式化常规类型, int, long, float, double
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="property"></param>
        /// <param name="value"></param>
        /// <param name="checkValid"></param>
        /// <returns></returns>
        static string FormatNormalValue(string fieldName, string property, string value, Predicate<string> checkValid)
        {
            string str = string.IsNullOrEmpty(value) ? "0" : value;
            if (checkValid != null)
            {
                if (!checkValid(str))
                    ThrowPropertyValue(fieldName, property, str);
            }

            return str;
        }

        /// <summary>
        /// 格式化常规类型数组, int[], long[], float[], double[]，不支持则抛出异常
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="property"></param>
        /// <param name="value"></param>
        /// <param name="checkValid"></param>
        /// <returns></returns>
        static string FormatNormalArray(string fieldName, string property, string value, Predicate<string> checkValid)
        {
            string invalidStr = null;
            bool ret = FormatNormalArray(value, checkValid, out var str, ref invalidStr);
            if (ret)
                return str;

            ThrowPropertyValue(fieldName, property, invalidStr);
            return str;
        }

        /// <summary>
        /// 格式化常规类型数组，int[], long[], float[], double[]
        /// </summary>
        /// <param name="value"></param>
        /// <param name="checkValid"></param>
        /// <param name="result"></param>
        /// <param name="invalidStr"></param>
        /// <returns></returns>
        static bool FormatNormalArray(string value, Predicate<string> checkValid, out string result, ref string invalidStr)
        {
            result = "null";
            if (string.IsNullOrEmpty(value))
                return true;

            string str = value.Replace(Delimiter, ",");
            if (str.EndsWith(","))
                str += "0";

            if (checkValid != null)
            {
                string[] values = str.Split(',');
                foreach (var v in values)
                {
                    if (!checkValid(v))
                    {
                        invalidStr = v;
                        return false;
                    }
                }
            }            

            str = "[" + str + "]";
            result = str;
            return true;
        }

        /// <summary>
        /// 格式化字符串数组
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        static string FormatStringArray(string configName, string id, string fieldName, string property, string value)
        {
            if (string.IsNullOrEmpty(value))
                return "null";

            string[] strs = value.Split(Delimiter);
            string str = string.Empty;
            for (int i = 0; i < strs.Length; i++)
            {
                string v = FormatLanguageKey(configName, id, fieldName, property, strs[i], i);
                if (i == 0)
                    str = v;
                else
                    str += "," + v;
            }
            str = "[" + str + "]";
            return str;
        }

        /// <summary>
        /// 获取多语言的Key
        /// </summary>
        /// <param name="configName"></param>
        /// <param name="id"></param>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        static string FormatLanguageKey(string configName, string id, string fieldName, string property, string value, int index = -1)
        {
            if (!FormatProperty(ref property))
                return $"\"{value}\"";

            if (string.IsNullOrEmpty(value))
                return $"\"{value}\"";

            // 如果多语言表里存在这个值，则使用这个值的Key
            if (LanguageHelper.TryGetKeyByValue(value, out var key))
                return $"\"${key}$\"";

            // 如果新添加的多语言里存在这个值，则使用这个值的key
            if (languageValueKey.TryGetValue(value, out key))
                return $"\"${key}$\"";

            // key由这几个字段组成，这样就不会重复
            key = string.Format("{0}_{1}_{2}", configName, fieldName, id);
            var copykey = key;
            if (index >= 0)
                copykey += $"_{index}";

            // 如果多语言表里包含了这个key，则顺延
            while (LanguageHelper.TryGetValueByKey(copykey, out _))
            {
                index++;
                if (index < 0)
                    index = 0;
                copykey = $"{key}_{index}";
            }

            key = copykey;

            // 添加到多语言字典
            languageKeyValue.Add(key, value);
            languageValueKey.Add(value, key);

            return $"\"${key}$\"";
        }

        /// <summary>
        /// 初始化空数组
        /// </summary>
        /// <param name="sheetInfos"></param>
        /// <returns></returns>
        static string FormatInitArray(List<SheetInfo> sheetInfos)
        {
            var sb = new StringBuilder();
            foreach (var sheetInfo in sheetInfos)
            {
                string property = FormatProperty(sheetInfo.property);
                string fieldName = sheetInfo.fieldName;
                if (property.EndsWith("[]"))
                {
                    string type = property.Substring(0, property.Length - 2);
                    sb.AppendLine($"\t\t\t{fieldName} ??= System.Array.Empty<{type}>();");
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// 导出二进制文件
        /// </summary>
        /// <exception cref="Exception"></exception>
        static void ExportExcelProtobuf()
        {
            List<string> classNameList = new List<string>();
            //string classPath = thisOutputClassPath;

            string outputPath = GetOutputConfigPath();
            if (!Directory.Exists(outputPath))
            {
                Directory.CreateDirectory(outputPath);
            }
            //foreach (string classFile in Directory.GetFiles(classPath, "*.cs"))
            //{
            //    classNameList.Add(Path.GetFileNameWithoutExtension(classFile));
            //}

            if (File.Exists(DiffFilePath))
            {
                diffConfigList = JsonConvert.DeserializeObject<HashSet<string>>(File.ReadAllText(DiffFilePath));
            }

            long time = 0;
            //foreach (var className in classNameList)
            foreach (var className in diffConfigList)
            {
                Type managerType = Type.GetType($"XFramework.{className}Manager");
                Type configType = Type.GetType($"XFramework.{className}");

                System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                try
                {
                    sw.Restart();

                    ProtoBuf.Serializer.NonGeneric.PrepareSerializer(managerType);
                    ProtoBuf.Serializer.NonGeneric.PrepareSerializer(configType);
                    string json = File.ReadAllText(Path.Combine(JsonPath, $"{className}.txt"));
                    object configObject = JsonConvert.DeserializeObject(json, managerType);

                    // 初始化
                    if (configObject is System.ComponentModel.ISupportInitialize obj)
                    {
                        obj.EndInit();
                    }

                    string path = Path.Combine(outputPath, $"{className}Manager.bytes");
                    using FileStream file = File.Create(path);
                    ProtoBuf.Serializer.Serialize(file, configObject);

                    sw.Stop();

                    time += sw.ElapsedMilliseconds;
                    Console.WriteLine($"序列化{configType}，耗时{sw.ElapsedMilliseconds}毫秒");

                    diffConfigList.Remove(className);
                }
                catch (Exception e)
                {
                    string diffJson = JsonConvert.SerializeObject(diffConfigList, Formatting.Indented);
                    File.WriteAllText(DiffFilePath, diffJson);
                    throw new Exception($"请检查{className}配置 -> {e}");
                }
            }

            if (File.Exists(DiffFilePath))
                File.Delete(DiffFilePath);

            Console.WriteLine($"总耗时{time / 1000f}秒");
        }
    }
}
