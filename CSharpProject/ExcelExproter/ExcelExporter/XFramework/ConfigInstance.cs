﻿using Newtonsoft.Json;
using ProtoBuf;
using System.Collections.Generic;

namespace XFramework
{
    [ProtoContract]
    [Config]
    public abstract class ConfigInstance<T> : ConfigObject where T : ProtoObject, IConfig
    {
        [ProtoIgnore]
        protected Dictionary<int, T> dictConfigs = new Dictionary<int, T>();

        protected abstract List<T> _list { get; }

        public sealed override void EndInit()
        {
            if (_list is null)
                return;

            foreach (T config in _list)
            {
                dictConfigs.Add(config.ConfigId, config);
                config.EndInit();
            }

            AfterEndInit();
        }
    }
}
