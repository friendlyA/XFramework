# 进阶版Unity C#框架

#### 介绍
master分支(视频版本)远远不够用，于是有了这个版本(advanced)。

#### 软件架构
面向对象编程，Unity版本需要2020.3.23或以上


#### 安装教程

无需安装，下载即用

#### 使用说明

入口场景为Init，入口类为Init.cs

#### 功能如下：

1.	资源加载，支持 <b>Resources</b> 和 <b>Addressable</b> 来加载资源，还预留了接口可以使用其他插件，注意，默认情况下是通过 <b>Addressable</b> 加载，详见 <b>ResourcesManager</b> 类<br><br>
2.	场景加载，支持添加到 <b>Build Settings</b> 里的场景和 <b>Addressable</b> 来加载场景，还预留了接口可以使用其他插件，注意，默认情况下是通过 <b>Addressable</b> 加载，详见 <b>SceneResManager</b> 类<br><br>
3.	支持Excel配置表导出到 <b>Unity</b> 和 <b>C#</b> ，详情请看 <b>Tools</b> 目录里的导表须知<br><br>
4.	资源绑定以及自动回收，<b>GameObject</b> 对象池和类对象池<br><br>
5.	全面UI管理，数据结构为树状结构，方便好用<br><br>
6.	事件系统, 事件接口 <b>IEvent(T)</b>，事件分发类 <b>EventManager</b><br><br>
7.	本地数据存档，数据结构为树状结构，定时自动存档，详见 <b>UserDataManager</b> 类<br><br>
8.	定时器，可以延迟执行，可定时执行等等操作，详见 <b>TimerManager</b> 类<br><br>
9.	多语言系统，支持运行时切换<br><br>
10.	UI音频管理，详见 <b>AudioManager</b> 类<br><br>
11. 线性补间动画，详见 <b>MiniTweenManager</b> 类<br><br>
12. 红点系统，数据结构为树状结构 详见 <b>RedDotManager</b> 类<br><br>
13.	敬请期待<br>


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
