cd ../CSharpProject/ExcelExproter/Bin
dotnet ExcelExporter.dll exportAll

if %errorlevel% neq 0 (goto error) else goto build

:error
pause
exit

:build
cls
dotnet build ../ExcelExporter/ExcelExporter.csproj
if %errorlevel% neq 0 (goto error) else goto continue

:continue
cls
dotnet ExcelExporter.dll serialize
pause