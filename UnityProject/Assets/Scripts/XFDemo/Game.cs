using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace XFramework
{
    public class Game : Singleton<Game>, IDisposable
    {
        private IEntry entry;

        public void Start()
        {
            entry = new DemoEntry();  //可以实现其他entry
            entry.Start();
        }

        public void Update()
        {
            entry?.Update();
        }

        public void LateUpdate()
        {
            entry?.LateUpdate();
        }

        public void FixedUpdate()
        {
            entry?.FixedUpdate();
        }

        public void Dispose()
        {
            entry?.Dispose();
            Instance = null;
        }
    }
}
