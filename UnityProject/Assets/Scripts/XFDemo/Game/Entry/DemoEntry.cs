﻿namespace XFramework
{
    public class DemoEntry : XFEntry
    {
        public override void Start()
        {
            LoadAsync().Coroutine();
        }

        private async XFTask LoadAsync()
        {
            var cover = UnityEngine.GameObject.Find("Cover");
            cover?.SetViewActive(true);

            // 加载资源前必须要设置Loader
            ResourcesManager.Instance.SetLoader(new AAResourcesLoader());   // 资源管理，设置加载方式
            SceneResManager.Instance.SetLoader(new AASceneLoader());    // 场景资源管理，设置加载方式

            base.Init();

            var configMgr = Common.Instance.Get<ConfigManager>();   // 配置表管理
            configMgr.SetLoader(new AAConfigLoader());  // 设置配置加载方式
            await configMgr.LoadAllConfigsAsync();     // 等待配置表加载完毕才能执行下面的内容

            Common.Instance.Get<LanguageManager>().SetLoader(new XLanguageLoader());    // 设置多语言加载方式
            UserDataHelper.SetCreateUser(UserHelper.CreateUser);   // 设置创建数据方法，只有在没有存档的时候才会执行创建User
            await UserDataHelper.LoadAsync();  //加载存档，不一定要写在这，写在你需要的地方即可

            Common.Instance.Get<RedDotManager>().InitAllRedNodes(); //初始化红点系统，红点刷新肯定依赖数据，所以得在数据初始化执行

            var sceneController = Common.Instance.Get<SceneController>();  // 场景控制
            var sceneObj = sceneController.LoadSceneAsync<StartScene>(SceneName.Start);
            await SceneResManager.WaitForCompleted(sceneObj);   // 等待场景加载完毕
            cover?.SetViewActive(false);
        }
    }
}
