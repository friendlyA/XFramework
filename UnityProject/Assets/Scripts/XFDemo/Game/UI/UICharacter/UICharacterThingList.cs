﻿using System.Collections.Generic;

namespace XFramework
{
    public class UICharacterThingList : UI, IAwake, ILoopScrollRectProvide<UIThingItem>
    {
        private class ThingData
        {
            public int Id { get; set; }

            public bool Grayed { get; set; }
        }

        /// <summary>
        /// 假设这是我们的物品数据
        /// </summary>
        private static readonly List<ThingData> thingList = new List<ThingData>(100);

        private static readonly Dictionary<int, ThingData> thingDict = new Dictionary<int, ThingData>(100);

        static UICharacterThingList()
        {
            // 随便设置一些假的物品数据演示用
            for (int i = 0; i < 100; i++)
            {
                var data = new ThingData { Id = 10000 + i };
                thingList.Add(data);
                thingDict.Add(data.Id, data);
            }
        }

        public void Initialize()
        {
            var loopRect = this.GetLoopScrollRect();
            loopRect.SetProvideData(UIPathSet.UIThingItem, this);
            loopRect.SetTotalCount(thingList.Count);
            loopRect.RefillCells();
        }

        protected override void OnClose()
        {
            base.OnClose();
        }

        public bool IsGrayed(int id)
        {
            return thingDict.Get(id)?.Grayed ?? false;
        }

        public void SetGrayed(int id, bool grayed)
        {
            if (thingDict.TryGetValue(id, out var data))
            {
                data.Grayed = grayed;
            }                
        }

        void ILoopScrollRectProvide<UIThingItem>.ProvideData(UIThingItem ui, int index)
        {
            var id = thingList[index].Id;
            ui.UpdateView(id);
        }
    }
}
