namespace XFramework
{
    [UIEvent(UIType.UIEquipItem)]
    internal sealed class UIEquipItemEvent : AUIEvent
    {
	    public override string Key => UIPathSet.UIEquipItem;

        public override bool IsFromPool => true;

		public override bool AllowManagement => false;

		public override UI OnCreate()
        {
            return UI.Create<UIEquipItem>(true);
        }
    }

    public partial class UIEquipItem : UIChild, IAwake
    {
		private int equipId;

        public void Initialize()
        {
            
        }

        public void UpdateView(int thingId)
		{
			this.SetId(thingId);
			this.equipId = thingId;
			this.RefreshView();
		}

		private void RefreshView()
		{
			string icon = this.equipId % 2 == 0 ? "Layer 207" : "Layer 208";
			this.GetImage(KIcon).SetSprite(icon, true);
		}

		protected override void OnClose()
		{
			this.equipId = 0;
			base.OnClose();
		}
	}
}
