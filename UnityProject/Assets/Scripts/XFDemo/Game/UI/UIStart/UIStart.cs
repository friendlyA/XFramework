using XFramework.UIEventType;

namespace XFramework
{
    [UIEvent(UIType.UIStart)]
    internal sealed class UIStartEvent : AUIEvent, IUILayer
    {
        public override string Key => UIPathSet.UIStart;

        public override bool IsFromPool => true;

        public override bool AllowManagement => true;

        public UILayer Layer => UILayer.Low;

        public override UI OnCreate()
        {
            return UI.Create<UIStart>();
        }
    }

    public partial class UIStart : UI, IAwake
	{
        private struct CloseSetting : IWaitType
        {
            public int Error { get; set; }
        }

        public void Initialize()
		{
            TestLanguage();
            this.GetButton(KSetting)?.OnClick.Add(OpenSetting);
            this.GetButton(KPlay)?.OnClick.Add(GoMainScene);
            this.GetButton(KGame)?.OnClick.Add(OpenHRD);
            //this.GetRedDot(KGame)?.RegisterRedDotEvent(RedDotConst.TestRedDot); //红点监听

            TestUIController().Coroutine();
            TestUIControlCenter().Coroutine();
        }

        private async XFTask TestUIController()
        {
            var tagId = this.TagId;
            var uiController = this.GetUIController(KPlay);
            var ctl = uiController.GetCtlGroupAt(0);

            if (ctl.Count == 0)
                return;

            //uiController.OnChanged.Set(i => Log.Info("ctl 更新 Index : {0}", ctl.SelectIndex));
            for (int i = 0; i < 10000000 + 1; i++)
            {
                ctl.SetIndex(i % ctl.Count);
                await TimerManager.Instance.WaitAsync(1000);
                if (tagId != this.TagId)
                    return;
            }
        }

        private async XFTask TestUIControlCenter()
        {
            var tagId = this.TagId;
            var uiControlCenter = this.GetUIControlCenter().Get();
            var size = uiControlCenter.Count;

            if (size == 0)
                return;

            //uiControlCenter.OnChanged.Set(() => Log.Info("UIControlCenter 更新 Index : {0}", uiControlCenter.GetSelectIndex()));

            for (int i = 0; i < size * 10000 + 1; i++)
            {
                uiControlCenter.SetIndex(i % size);
                await TimerManager.Instance.WaitAsync(500);
                if (tagId != this.TagId)
                    return;
            }
        }

        private void TestLanguage()
        {
            var txtComp = this.GetText(KTxtTestKey);
            var tmpComp = this.GetTextMeshPro(KTmpTestKey);
            using var list = XList<object>.Create();

            foreach (var item in TestConfigManager.Instance.GetAllValues())
            {
                foreach (var name in item.City)
                {
                    list.Add(name);
                }
            }

            string key = string.Empty;
            for (int i = 0; i < list.Count; i++)
            {
                if (i == 0)
                    key = $"{{{i}}}";
                else
                    key += $"__{{{i}}}";
            }

            txtComp.SetTextWithKey(key, list.ToArray());
            tmpComp.SetTextWithKey(key, list.ToArray());
        }

        private void OpenSetting()
        {
            AudioManager.Instance.PlaySFXAudio("Click");    //建议写一个统一的const string字段代替这个Click
            OpenSettingAsync().Coroutine();
        }

        private async XFTask OpenSettingAsync()
        {
            var tag = this.TagId;
            var ui = await UIHelper.CreateAsync(UIType.UISetting);
            if (ui is null || tag != this.TagId) // 异步方法如果后续要处理要用tag做判断
                return;

            // UISetting这个类继承了IWaitObject
            var ret = await ((IWaitObject)ui).Wait<CloseSetting>(); // 等待UISetting关闭
            if (ret.Error == WaitTypeError.Destroy) // 当关闭了UISetting后
            {
                Log.Info("关闭了设置界面");
            }
        }

        private void OpenHRD()
        {
            UIHelper.Create(UIType.UIHRD);
        }

        private void GoMainScene()
        {
            var sceneManager = Common.Instance.Get<SceneController>();
            sceneManager.LoadSceneAsync<MainScene>(SceneName.Main);
        }

		protected override void OnClose()
		{
			
		}
    }
}
