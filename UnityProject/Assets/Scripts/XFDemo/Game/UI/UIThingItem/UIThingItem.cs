namespace XFramework
{
    [UIEvent(UIType.UIThingItem)]
    internal sealed class UIThingItemEvent : AUIEvent
    {
	    public override string Key => UIPathSet.UIThingItem;

        public override bool IsFromPool => true;

		public override bool AllowManagement => false;

		public override UI OnCreate()
        {
            return UI.Create<UIThingItem>(true);
        }
    }

    public partial class UIThingItem : UIChild, IAwake
	{
		private int thingId;

		private UICharacterThingList parentUI => Parent.GetParent<UICharacterThingList>();

        public void Initialize()
        {
            this.GetButton().OnClick.Set(ClickEvent);
        }

        public void UpdateView(int thingId)
		{
			this.SetId(thingId);
			this.thingId = thingId;
            this.RefreshView();
        }

        private void RefreshView()
        {
            string icon = this.thingId % 2 == 0 ? "item copy" : "item";
            this.GetImage(KIcon).SetSprite(icon, true);
            this.SetGrayed(parentUI.IsGrayed(thingId));
        }

		private void ClickEvent()
		{
            var parent = parentUI;
			var grayed = parent.IsGrayed(thingId);

			parent.SetGrayed(thingId, !grayed);
			this.SetGrayed(!grayed);

            //Log.Error((Parent.GetList().GetChildById(Id) == this).ToString());
		}
		
		protected override void OnClose()
		{
			this.thingId = 0;
			base.OnClose();
		}
    }
}
