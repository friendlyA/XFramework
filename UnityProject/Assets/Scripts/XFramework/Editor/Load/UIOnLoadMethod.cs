using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEditor;
using System.Reflection;
using System.Collections.Generic;
using System;
using Object = UnityEngine.Object;

namespace XFramework.Editor
{
    internal class UIOnLoadMethod
    {
        [AttributeUsage(AttributeTargets.Class)]
        class InitializeCompClassAttribute : Attribute
        {

        }

        interface IInitializeComponent
        {
            void Handle(Component component);

            Type GetCompType();
        }

        [InitializeCompClass]
        abstract class InitializeComponent<T> : IInitializeComponent where T : Component
        {
            public Type GetCompType()
            {
                return typeof(T);
            }

            public void Handle(Component component)
            {
                Run((T)component);
            }

            protected abstract void Run(T comp);
        }

        class InitText : InitializeComponent<Text>
        {
            protected override void Run(Text comp)
            {
                comp.raycastTarget = false;
            }
        }

        private static Dictionary<Type, (int Order, Type ReplaceType)> replaceTypeDict = new Dictionary<Type, (int, Type)>();
        private static Dictionary<Type, IInitializeComponent> initCompDict = new Dictionary<Type, IInitializeComponent>();

        [InitializeOnLoadMethod]
        private static void InitializeCompnent()
        {
            InitReplaceAttris();
            InitCompMethod();
            UnityEditor.ObjectFactory.componentWasAdded += ComponentWasAdded;
        }

        private static void InitReplaceAttris()
        {
            replaceTypeDict.Clear();
            foreach (var ass in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                if (ass.GetName().Name.Contains("Editor"))
                    continue;

                foreach (var type in ass.GetTypes())
                {
                    if (type.IsAbstract)
                        continue;

                    var attri = type.GetCustomAttribute<ReplaceComponentAttribute>();
                    if (attri == null || attri.OriginType == null)
                        continue;                        

                    if (replaceTypeDict.TryGetValue(attri.OriginType, out var item))
                    {
                        if (attri.Order <= item.Order)
                            continue;
                    }

                    if (attri.OriginType == type)
                    {
                        Debug.LogError("ReplaceComponentAttribute 禁止使用相同类");
                        continue;
                    }

                    replaceTypeDict[attri.OriginType] = (attri.Order, type);
                }
            }
        }

        private static void InitCompMethod()
        {
            initCompDict.Clear();
            foreach (var type in typeof(UIOnLoadMethod).Assembly.GetTypes())
            {
                if (type.IsAbstract)
                    continue;

                if (type.GetCustomAttribute<InitializeCompClassAttribute>() == null)
                    continue;

                var obj = Activator.CreateInstance(type) as IInitializeComponent;
                if (obj == null)
                    continue;

                initCompDict.Add(obj.GetCompType(), obj);
            }
        }

        /// <summary>
        /// 创建组件后执行的方法
        /// </summary>
        /// <param name="component"></param>
        private static void ComponentWasAdded(Component component)
        {
            var orginType = component.GetType();
            if (!replaceTypeDict.TryGetValue(orginType, out var item))
            {
                HandleInit(orginType, component);
                return;
            }

            var gameObject = component.gameObject;
            Object.DestroyImmediate(component);
            var newComp = gameObject.AddComponent(item.ReplaceType);

            HandleInit(orginType, newComp);
            HandleInit(item.ReplaceType, newComp);
        }

        private static void HandleInit(Type type, Component component)
        {
            foreach (var item in initCompDict.Keys)
            {
                if (item.IsAssignableFrom(type))
                {
                    initCompDict[item].Handle(component);
                }
            }
        }

        #region
        /// <summary>
        /// 创建组件后执行的方法
        /// </summary>
        /// <param name="component"></param>
        [Obsolete("已过时", true)]
        private static void ComponentWasAdded1(Component component)
        {
            if (component is Button && !(component is XButton))
            {
                GameObject obj = component.gameObject;
                Object.DestroyImmediate(component, true);
                obj.AddComponent<XButton>();
                Image image = obj.GetComponent<Image>();
                if (image)
                    image.raycastTarget = true;
            }
            else if (component is Image img)
            {
                bool notExist = !img.GetComponent<Button>() && !img.GetComponentInParent<Toggle>();
                img.raycastTarget = !notExist;

                if (component is XImage)
                    return;

                GameObject obj = component.gameObject;
                Object.DestroyImmediate(component, true);
                obj.AddComponent<XImage>();
            }
            else if (component is Text && !(component is XText))
            {
                GameObject obj = component.gameObject;
                Object.DestroyImmediate(component, true);
                XText xt = obj.AddComponent<XText>();
                if (!xt.GetComponentInParent<InputField>())
                    xt.raycastTarget = false;
            }
            else if (component is Toggle && !(component is XToggle))
            {
                GameObject obj = component.gameObject;
                Object.DestroyImmediate(component, true);
                obj.AddComponent<XToggle>();
            }
            else if (component is InputField input)
            {
                if (!(input is XInputField))
                {
                    GameObject obj = component.gameObject;
                    Object.DestroyImmediate(component, true);
                    obj.AddComponent<XInputField>();
                    return;
                }

                Image image = input.GetComponent<Image>();
                if (image)
                    image.raycastTarget = true;
            }
            else if (component is ScrollRect scroll)
            {
                if (!(scroll is XScrollRect))
                {
                    GameObject obj = component.gameObject;
                    Object.DestroyImmediate(component, true);
                    obj.AddComponent<XScrollRect>();
                    return;
                }

                Image image = scroll.GetComponent<Image>();
                if (image)
                    image.raycastTarget = true;
            }
            else if (component is Slider slider)
            {
                if (component is XSlider)
                    return;

                GameObject obj = component.gameObject;
                Object.DestroyImmediate(component, true);
                obj.AddComponent<XSlider>();
            }
            else if (component is Dropdown dropdown)
            {
                if (!(component is XDropdown))
                {
                    GameObject obj = component.gameObject;
                    Object.DestroyImmediate(component, true);
                    obj.AddComponent<XDropdown>();
                    return;
                }

                Image image = dropdown.GetComponent<Image>();
                if (image)
                    image.raycastTarget = true;
            }
            else if (component is TextMeshPro tmp)
            {
                if (!(component is XTextMeshPro))
                {
                    GameObject obj = component.gameObject;
                    Object.DestroyImmediate(component, true);
                    obj.AddComponent<XTextMeshPro>();
                    return;
                }
            }
            else if (component is TextMeshProUGUI tmpUGUI)
            {
                if (!(component is XTextMeshProUGUI))
                {
                    GameObject obj = component.gameObject;
                    Object.DestroyImmediate(component, true);
                    obj.AddComponent<XTextMeshProUGUI>();
                    return;
                }
            }
        }
        #endregion
    }
}
