﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace XFramework.Editor
{
    [CustomEditor(typeof(UIControlCenter))]
    public class UIControlCenterEditor : UnityEditor.Editor
    {
        private const string OnChangedName = "m_OnChanged";

        private const string ControlDatasName = "controlDatas";

        private const string Name = "Name";

        private const string SelectIndexName = "SelectIndex";

        private const string Ctl = "Ctl";

        private const string ControllerIndexList = "ControllerIndexList";

        private const string CtlCenter = "CtlCenter";

        private const string ControlIndexList = "ControlIndexList";

        private SerializedProperty onChangedProperty;

        private SerializedProperty controlDatas;

        private SerializedProperty selectIndex;

        private int controlDatasSize;

        private List<string> optionList;

        private List<string> optionList2;

        private UIControlCenter uiControlCenter;

        private bool IsPlaying()
        {
            return Application.IsPlaying(target);
        }

        private void OnEnable()
        {
            optionList = new List<string>();
            optionList2 = new List<string>();
            onChangedProperty = serializedObject.FindProperty(OnChangedName);
            controlDatas = serializedObject.FindProperty(ControlDatasName);
            selectIndex = serializedObject.FindProperty(nameof(selectIndex));
            controlDatasSize = controlDatas.arraySize;
            uiControlCenter = target as UIControlCenter;
        }

        public override void OnInspectorGUI()
        {
            GUILayout.BeginHorizontal();
            controlDatasSize = Math.Max(1, EditorGUILayout.IntField("Size", controlDatasSize));
            if (GUILayout.Button("添加", GUILayout.Width(50)))
            {
                controlDatas.arraySize++;
                controlDatasSize = controlDatas.arraySize;
            }
            GUILayout.EndHorizontal();
            if (!IsPlaying())
            {
                if (selectIndex.intValue >= controlDatasSize)
                {
                    selectIndex.intValue = 0;
                    Apply();
                }
            }

            optionList.Clear();
            if (controlDatasSize > 0)
            {
                for (int i = 0; i < controlDatasSize; i++)
                {
                    var controller = controlDatas.GetArrayElementAtIndex(i);
                    var name = controller.FindPropertyRelative(Name).stringValue.Trim();
                    if (!string.IsNullOrEmpty(name))
                        optionList.Add($"{i} : {name}");
                    else
                        optionList.Add(i.ToString());
                }

                GUILayout.BeginHorizontal();

                var index = selectIndex.intValue;
                index = EditorGUILayout.Popup(SelectIndexName, index, optionList.ToArray());
                if (index != selectIndex.intValue)
                {
                    selectIndex.intValue = index;
                    Apply();
                    uiControlCenter.UpdateState();
                }
                if (GUILayout.Button("刷新", GUILayout.Width(50)))
                {
                    uiControlCenter.UpdateState();
                }

                GUILayout.EndHorizontal();
            }

            controlDatas.isExpanded = EditorGUILayout.Foldout(controlDatas.isExpanded, "ControlDatas", true);
            if (controlDatas.isExpanded && controlDatasSize > 0)
            {
                EditorGUI.indentLevel++;
                for (int i = 0; i < controlDatas.arraySize; i++)
                {
                    var isRemove = false;
                    GUILayout.BeginHorizontal();
                    var data = controlDatas.GetArrayElementAtIndex(i);
                    data.isExpanded = EditorGUILayout.Foldout(data.isExpanded, $"Element {i}", true);
                    if (controlDatas.arraySize > 1)
                    {
                        if (GUILayout.Button("移除", GUILayout.Width(50)))
                        {
                            isRemove = true;
                            controlDatas.DeleteArrayElementAtIndex(i);
                            i--;
                            controlDatasSize--;
                        }
                    }         
                    GUILayout.EndHorizontal();

                    if (isRemove || !data.isExpanded)
                        continue;

                    DrawControlDatas(i);
                }
                EditorGUI.indentLevel--;
            }

            onChangedProperty.isExpanded = EditorGUILayout.Foldout(onChangedProperty.isExpanded, onChangedProperty.displayName, true);
            if (onChangedProperty.isExpanded)
            {
                EditorGUILayout.Space();
                EditorGUILayout.PropertyField(onChangedProperty);
            }

            Apply();
        }

        private void DrawControlDatas(int index)
        {
            var data = controlDatas.GetArrayElementAtIndex(index);
            EditorGUI.indentLevel++;

            var name = data.FindPropertyRelative("Name");
            var controllerList = data.FindPropertyRelative("ControllerList");
            var controlCenterList = data.FindPropertyRelative("ControlCenterList");

            DrawDragger(controllerList, controlCenterList);
            EditorGUILayout.PropertyField(name);
            DrawControllerList(controllerList);
            DrawControlCenterList(controlCenterList);

            EditorGUI.indentLevel--;
        }

        private void DrawDragger(params SerializedProperty[] dataList)
        {
            var uiControllerObj = (UIController)EditorGUILayout.ObjectField($"Drag {nameof(UIController)}", null, typeof(UIController), this);
            var uiControlCenterObj = (UIControlCenter)EditorGUILayout.ObjectField($"Drag {nameof(UIControlCenter)}", null, typeof(UIControlCenter), this);
            if (uiControllerObj)
            {
                AddUIController(dataList[0], uiControllerObj);
            }
            if (uiControlCenterObj)
            {
                AddUIControlCenter(dataList[1], uiControlCenterObj);
            }
        }

        private void AddUIController(SerializedProperty data, UIController controller)
        {
            for (int i = 0; i < data.arraySize; i++)
            {
                if (data.GetArrayElementAtIndex(i).FindPropertyRelative(Ctl).objectReferenceValue == controller)
                    return;
            }

            data.arraySize++;
            var p = data.GetArrayElementAtIndex(data.arraySize - 1);
            p.FindPropertyRelative(Ctl).objectReferenceValue = controller;
            var indexList = p.FindPropertyRelative(ControllerIndexList);
            indexList.ClearArray();
            indexList.arraySize = 1;
        }

        private void AddUIControlCenter(SerializedProperty data, UIControlCenter controlCenter)
        {
            for (int i = 0; i < data.arraySize; i++)
            {
                if (data.GetArrayElementAtIndex(i).FindPropertyRelative(CtlCenter).objectReferenceValue == controlCenter)
                    return;
            }

            data.arraySize++;
            var p = data.GetArrayElementAtIndex(data.arraySize - 1);
            p.FindPropertyRelative(CtlCenter).objectReferenceValue = controlCenter;
            var indexList = p.FindPropertyRelative(ControlIndexList);
            indexList.ClearArray();
            indexList.arraySize = 1;
        }

        private void DrawControllerList(SerializedProperty list)
        {
            list.isExpanded = EditorGUILayout.Foldout(list.isExpanded, list.displayName, true);
            if (list.isExpanded)
            {
                EditorGUI.indentLevel++;

                for (int i = 0; i < list.arraySize; i++)
                {
                    var p = list.GetArrayElementAtIndex(i);
                    var ctl = p.FindPropertyRelative(Ctl);
                    var indexList = p.FindPropertyRelative(ControllerIndexList);
                    var isRemove = false;

                    if (i > 0)
                        EditorGUILayout.Space();

                    GUILayout.BeginHorizontal();
                    GUI.enabled = false;
                    EditorGUILayout.PropertyField(ctl);
                    GUI.enabled = true;
                    if (GUILayout.Button("添加", GUILayout.Width(50)))
                    {
                        indexList.arraySize++;
                    }
                    if (GUILayout.Button("清空", GUILayout.Width(50)))
                    {
                        list.DeleteArrayElementAtIndex(i);
                        i--;
                        isRemove = true;
                    }
                    GUILayout.EndHorizontal();

                    if (isRemove)
                        continue;

                    DrawControllerIndexList(ctl.objectReferenceValue as UIController, indexList);
                }

                EditorGUI.indentLevel--;
            }
        }

        private void DrawControllerIndexList(UIController ctl, SerializedProperty list)
        {
            if (!ctl)
            {
                list.arraySize = 0;
                return;
            }

            const string Index = "Index";
            const string ChildIndex = "ChildIndex";
            const string Controllers = "Controllers";

            list.isExpanded = EditorGUILayout.Foldout(list.isExpanded, "List", true);

            EditorGUI.indentLevel++;

            var serializeObj = new SerializedObject(ctl);
            var controllerList = serializeObj.FindProperty("controllerList");

            optionList.Clear();
            optionList2.Clear();

            for (int i = 0; i < list.arraySize; i++)
            {
                optionList.Clear();
                optionList2.Clear();

                var p = list.GetArrayElementAtIndex(i);
                var isRemove = false;

                if (list.isExpanded)
                {
                    if (i > 0)
                        EditorGUILayout.Space();

                    GUILayout.BeginHorizontal();

                    p.isExpanded = EditorGUILayout.Foldout(p.isExpanded, $"Element {i}", true);
                    if (GUILayout.Button("移除元素", GUILayout.Width(100)))
                    {
                        isRemove = true;
                        list.DeleteArrayElementAtIndex(i);
                        i--;
                    }

                    GUILayout.EndHorizontal();

                    if (isRemove)
                        continue;
                }

                bool show = list.isExpanded && p.isExpanded;

                EditorGUI.indentLevel++;

                var index = p.FindPropertyRelative(Index);
                var childIndex = p.FindPropertyRelative(ChildIndex);
                if (index.intValue >= controllerList.arraySize)
                {
                    index.intValue = 0;
                    childIndex.intValue = 0;
                }

                var indexValue = index.intValue;
                for (int j = 0; j < controllerList.arraySize; j++)
                {
                    var ctlGroup = controllerList.GetArrayElementAtIndex(j);
                    var ctlList = ctlGroup.FindPropertyRelative(Controllers);
                    var name = ctlGroup.FindPropertyRelative(Name).stringValue.Trim();

                    if (indexValue == j && childIndex.intValue >= ctlList.arraySize)
                        childIndex.intValue = 0;

                    if (show)
                    {
                        if (!string.IsNullOrEmpty(name))
                            optionList.Add($"{j} : {name}");
                        else
                            optionList.Add(j.ToString());
                    }

                    if (show && indexValue == j)
                    {
                        for (int k = 0; k < ctlList.arraySize; k++)
                        {
                            var name1 = ctlList.GetArrayElementAtIndex(k).FindPropertyRelative(Name).stringValue.Trim();
                            if (!string.IsNullOrEmpty(name1))
                                optionList2.Add($"{k} : {name1}");
                            else
                                optionList2.Add(k.ToString());
                        }
                    }
                }

                if (show)
                {
                    GUILayout.BeginVertical();

                    if (optionList.Count > 0)
                    {
                        index.intValue = EditorGUILayout.Popup(Index, index.intValue, optionList.ToArray());
                    }
                    if (optionList2.Count > 0)
                    {
                        childIndex.intValue = EditorGUILayout.Popup(ChildIndex, childIndex.intValue, optionList2.ToArray());
                    }

                    GUILayout.EndVertical();
                } 

                EditorGUI.indentLevel--;

                optionList.Clear();
                optionList2.Clear();
            }

            EditorGUI.indentLevel--;
        }

        private void DrawControlCenterList(SerializedProperty list)
        {
            list.isExpanded = EditorGUILayout.Foldout(list.isExpanded, list.displayName, true);
            if (list.isExpanded)
            {
                EditorGUI.indentLevel++;

                for (int i = 0; i < list.arraySize; i++)
                {
                    var isRemove = false;
                    var p = list.GetArrayElementAtIndex(i);
                    var ctl = p.FindPropertyRelative(CtlCenter);
                    var indexList = p.FindPropertyRelative(ControlIndexList);

                    if (i > 0)
                        EditorGUILayout.Space();

                    GUILayout.BeginHorizontal();

                    GUI.enabled = false;
                    EditorGUILayout.PropertyField(ctl);
                    GUI.enabled = true;
                    if (GUILayout.Button("添加", GUILayout.Width(50)))
                    {
                        indexList.arraySize++;
                    }
                    if (GUILayout.Button("清空", GUILayout.Width(50)))
                    {
                        isRemove = true;
                        list.DeleteArrayElementAtIndex(i);
                        i--;
                    }

                    GUILayout.EndHorizontal();

                    if (isRemove)
                        continue;

                    DrawControlIndexList(ctl.objectReferenceValue as UIControlCenter, indexList);
                }

                EditorGUI.indentLevel--;
            }
        }

        private void DrawControlIndexList(UIControlCenter ctl, SerializedProperty list)
        {
            if (!ctl)
            {
                list.arraySize = 0;
                return;
            }

            const string Index = "Index";

            list.isExpanded = EditorGUILayout.Foldout(list.isExpanded, "List", true);

            EditorGUI.indentLevel++;

            var serializeObj = new SerializedObject(ctl);
            var controlDatas = serializeObj.FindProperty(ControlDatasName);

            optionList.Clear();

            for (int i = 0; i < list.arraySize; i++)
            {
                var isRemove = false;
                var index = list.GetArrayElementAtIndex(i);

                if (list.isExpanded)
                {
                    if (i > 0)
                        EditorGUILayout.Space();

                    GUILayout.BeginHorizontal();

                    index.isExpanded = EditorGUILayout.Foldout(index.isExpanded, $"Element {i}", true);
                    if (GUILayout.Button("移除元素", GUILayout.Width(100)))
                    {
                        isRemove = true;
                        list.DeleteArrayElementAtIndex(i);
                        i--;
                    }

                    GUILayout.EndHorizontal();

                    if (isRemove)
                        continue;
                }

                bool show = list.isExpanded && index.isExpanded;

                EditorGUI.indentLevel++;

                if (index.intValue >= controlDatas.arraySize)
                {
                    index.intValue = 0;
                }

                for (int j = 0; j < controlDatas.arraySize; j++)
                {
                    var ctlGroup = controlDatas.GetArrayElementAtIndex(j);
                    var name = ctlGroup.FindPropertyRelative(Name).stringValue.Trim();

                    if (show)
                    {
                        if (!string.IsNullOrEmpty(name))
                            optionList.Add($"{j} : {name}");
                        else
                            optionList.Add(j.ToString());
                    }
                }

                if (show)
                {
                    GUILayout.BeginVertical();

                    if (optionList.Count > 0)
                    {
                        index.intValue = EditorGUILayout.Popup(Index, index.intValue, optionList.ToArray());
                    }

                    GUILayout.EndVertical();
                }

                EditorGUI.indentLevel--;

                optionList.Clear();
            }

            EditorGUI.indentLevel--;
        }

        private void Apply()
        {
            serializedObject.ApplyModifiedProperties();
            //serializedObject.UpdateIfRequiredOrScript();
        }
    }
}
