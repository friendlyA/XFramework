﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace XFramework.Editor
{
    [CustomEditor(typeof(UIController))]
    public class UIControllerEditor : UnityEditor.Editor
    {
        private const string OnChangedName = "m_OnChanged";

        private const string ControllerListName = "controllerList";

        private const string ControllersName = "Controllers";

        private const string ControllerStateName = "State";

        private const string Name = "Name";

        private const string SelectIndexName = "SelectIndex";

        private SerializedProperty onChangedProperty;

        private SerializedProperty ctlListProperty;

        private UIController uiController;

        private int ctlListSize = 0;

        private List<string> optionList;

        private List<(string OriginName, string Tag)> stateNameList;

        private Action AddIndentLevel;

        private Action ReduceIndentLevel;

        private bool IsPlaying()
        {
            return Application.IsPlaying(target);
        }

        private void OnEnable()
        {
            AddIndentLevel = () => EditorGUI.indentLevel++;
            ReduceIndentLevel = () => EditorGUI.indentLevel--;
            optionList = new List<string>();
            stateNameList = new List<(string, string)>();
            onChangedProperty = serializedObject.FindProperty(OnChangedName);
            ctlListProperty = serializedObject.FindProperty(ControllerListName);
            ctlListSize = ctlListProperty.arraySize;
            uiController = target as UIController;
        }

        public override void OnInspectorGUI()
        {
            GUILayout.BeginHorizontal();
            var size = Math.Max(1, EditorGUILayout.IntField("Size", ctlListProperty.arraySize));
            if (GUILayout.Button("添加", GUILayout.Width(50)))
            {
                ctlListProperty.arraySize++;
                ctlListSize = ctlListProperty.arraySize;
            }
            GUILayout.EndHorizontal();

            ctlListProperty.isExpanded = EditorGUILayout.Foldout(ctlListProperty.isExpanded, "List", true);
            if (ctlListSize > 0)
            {
                EditorGUI.indentLevel++;
                for (int i = 0; i < ctlListProperty.arraySize; i++)
                {
                    var isRemove = false;
                    var controllerGroup = ctlListProperty.GetArrayElementAtIndex(i);

                    if (ctlListProperty.isExpanded)
                    {
                        EditorGUILayout.Space();
                        GUILayout.BeginHorizontal();
                        controllerGroup.isExpanded = EditorGUILayout.Foldout(controllerGroup.isExpanded, "Controller", true);
                        if (ctlListProperty.arraySize > 1 && GUILayout.Button("移除", GUILayout.Width(50)))
                        {
                            isRemove = true;
                            ctlListProperty.DeleteArrayElementAtIndex(i);
                            i--;
                            ctlListSize--;
                        }

                        GUILayout.EndHorizontal();
                    }

                    if (isRemove)
                        continue;

                    if (controllerGroup.isExpanded)
                        DrawControllerGroup(i);
                }
                EditorGUI.indentLevel--;
            }

            onChangedProperty.isExpanded = EditorGUILayout.Foldout(onChangedProperty.isExpanded, onChangedProperty.displayName, true);
            if (onChangedProperty.isExpanded)
            {
                EditorGUILayout.Space();
                EditorGUILayout.PropertyField(onChangedProperty);
            }

            Apply();
        }

        private void DrawControllerGroup(int index)
        {
            var controllerGroup = ctlListProperty.GetArrayElementAtIndex(index);
            var indexProperty = controllerGroup.FindPropertyRelative(SelectIndexName);
            var stateProperty = controllerGroup.FindPropertyRelative(ControllerStateName);
            var listProperty = controllerGroup.FindPropertyRelative(ControllersName);
            var nameProperty = controllerGroup.FindPropertyRelative(Name);
            var isExpanded = ctlListProperty.isExpanded && controllerGroup.isExpanded;

            if (isExpanded)
                EditorGUILayout.Space();

            bool isPlaying = IsPlaying();
            EditorGUI.indentLevel++;

            if (isPlaying)
                GUI.enabled = false;

            if (isExpanded)
            {
                EditorGUILayout.PropertyField(nameProperty);
                var selectIndex = indexProperty.intValue;
                GUILayout.BeginHorizontal();
                var size = Math.Max(1, EditorGUILayout.IntField("Size", listProperty.arraySize));
                if (GUILayout.Button("添加", GUILayout.Width(50)))
                {
                    listProperty.arraySize++;

                    // 新增的部分复制之前的属性
                    var len = listProperty.arraySize;
                    var origin = listProperty.GetArrayElementAtIndex(len - 1);
                    for (int i = len; i < size; i++)
                    {
                        CopyToProperty(origin, listProperty.GetArrayElementAtIndex(i), UIController.ControllerType,
                            0, typeof(UIControllerStateFlagAttribute), typeof(UIControllerStateValueAttribute));
                    }

                    Apply();
                }
                GUILayout.EndHorizontal();
            }

            if (listProperty.arraySize > 0)
            {
                var selectIndex = indexProperty.intValue;
                var option = optionList;
                option.Clear();

                if (isExpanded)
                {
                    for (int i = 0; i < listProperty.arraySize; i++)
                    {
                        var controller = listProperty.GetArrayElementAtIndex(i);
                        var name = controller.FindPropertyRelative(Name).stringValue.Trim();
                        if (!string.IsNullOrEmpty(name))
                            option.Add($"{i} : {name}");
                        else
                            option.Add(i.ToString());
                    }

                    GUI.enabled = true;

                    selectIndex = EditorGUILayout.Popup(SelectIndexName, selectIndex, option.ToArray());
                    if (selectIndex != indexProperty.intValue)
                    {
                        indexProperty.intValue = selectIndex;
                        Apply();
                        uiController.UpdateState(index);
                    }

                    if (isPlaying)
                        GUI.enabled = false;

                    GUILayout.BeginHorizontal();
                    listProperty.isExpanded = EditorGUILayout.Foldout(listProperty.isExpanded, "Properties", true);
                    if (listProperty.arraySize > 1 && GUILayout.Button("移除元素", GUILayout.Width(100)))
                    {
                        listProperty.DeleteArrayElementAtIndex(selectIndex);
                        selectIndex = 0;
                        indexProperty.intValue = selectIndex;

                        Apply();
                        uiController.UpdateState(index);
                    }
                    GUILayout.EndHorizontal();
                }

                if (listProperty.arraySize > selectIndex)
                {
                    DrawController(stateProperty, listProperty, selectIndex, isExpanded && listProperty.isExpanded);
                }
            }

            EditorGUI.indentLevel--;
        }

        private void DrawController(SerializedProperty state, SerializedProperty controllerList, int index, bool isExpanded)
        {
            EditorGUI.indentLevel++;
            var controller = controllerList.GetArrayElementAtIndex(index);
            var nameProperty = controller.FindPropertyRelative(Name);

            if (isExpanded)
            {
                EditorGUILayout.Space();
                EditorGUILayout.PropertyField(nameProperty);
            }

            BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            var fields = UIController.ControllerType.GetFields(flags);
            foreach (var field in fields)
            {
                var attri = field.GetCustomAttribute<UIControllerStateFlagAttribute>();
                if (attri == null || attri.TargetType == null)
                    continue;

                if (!DrawControllerField(state, controller, field.FieldType, field.Name, attri.TargetType, isExpanded))
                {
                    // 如果没有相应组件，则清除存储的数据
                    for (int i = 0; i < controllerList.arraySize; i++)
                    {
                        var p = controllerList.GetArrayElementAtIndex(i).FindPropertyRelative(field.Name);
                        foreach (var item in field.FieldType.GetFields(flags))
                        {
                            if (item.GetCustomAttribute<UIControllerStateValueAttribute>() == null)
                                continue;

                            UpdatePropertyValue(p.FindPropertyRelative(item.Name), null);
                        }
                    }
                }
            }

            EditorGUI.indentLevel--;
        }

        private bool DrawControllerField(SerializedProperty state, SerializedProperty controller, Type ctlType, string ctlName, Type targetType, bool isExpanded)
        {
            var property = controller.FindPropertyRelative(ctlName);
            if (property == null)
                return false;

            BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            UnityEngine.Object unityObject = null;
            if (targetType == typeof(GameObject))
                unityObject = uiController.gameObject;
            else
                unityObject = uiController.GetComponent(targetType);

            if (unityObject == null)
            {
                DeleteStateField(state, targetType.Name);
                return false;
            }

            if (isExpanded)
                DrawStateField(state, targetType.Name);

            if (IsPlaying())
                return true;

            var serialzedObj = new SerializedObject(unityObject);
            foreach (var field in ctlType.GetFields(flags))
            {
                var attri = field.GetCustomAttribute<UIControllerStateValueAttribute>();
                if (attri == null || attri.Custom)
                    continue;

                var p = property.FindPropertyRelative(field.Name);
                if (p == null)
                    continue;

                var f = serialzedObj.FindProperty(attri.FieldNames);
                if (f == null)
                {
                    Debug.LogError($"{targetType} no found Property {attri.FieldNames}");
                    continue;
                }

                UpdatePropertyValue(p, f);
            }

            return true;
        }

        private void UpdatePropertyValue(SerializedProperty origin, SerializedProperty target)
        {
            if (target != null && origin.propertyType != target.propertyType)
            {
                Debug.LogError($"{target.displayName} 的类型{target.propertyType}与{origin.displayName}的类型{origin.propertyType}不匹配");
                return;
            }

            switch (origin.propertyType)
            {
                case SerializedPropertyType.Enum:
                case SerializedPropertyType.Integer:
                    origin.intValue = target == null ? default : target.intValue;
                    break;
                case SerializedPropertyType.Boolean:
                    origin.boolValue = target == null ? default : target.boolValue;
                    break;
                case SerializedPropertyType.Float:
                    origin.floatValue = target == null ? default : target.floatValue;
                    break;
                case SerializedPropertyType.String:
                    origin.stringValue = target == null ? default : target.stringValue;
                    break;
                case SerializedPropertyType.Color:
                    origin.colorValue = target == null ? default : target.colorValue;
                    break;
                case SerializedPropertyType.ObjectReference:
                    origin.objectReferenceValue = target == null ? default : target.objectReferenceValue;
                    break;
                case SerializedPropertyType.Vector2:
                    origin.vector2Value = target == null ? default : target.vector2Value;
                    break;
                case SerializedPropertyType.Vector3:
                    origin.vector3Value = target == null ? default : target.vector3Value;
                    break;
                case SerializedPropertyType.Vector4:
                    origin.vector4Value = target == null ? default : target.vector4Value;
                    break;
                case SerializedPropertyType.Rect:
                    origin.rectValue = target == null ? default : target.rectValue;
                    break;
                case SerializedPropertyType.ArraySize:
                    origin.arraySize = target == null ? default : target.arraySize;
                    break;
                case SerializedPropertyType.AnimationCurve:
                    origin.animationCurveValue = target == null ? default : target.animationCurveValue;
                    break;
                case SerializedPropertyType.Bounds:
                    origin.boundsValue = target == null ? default : target.boundsValue;
                    break;
                case SerializedPropertyType.Quaternion:
                    origin.quaternionValue = target == null ? default : target.quaternionValue;
                    break;
                case SerializedPropertyType.Vector2Int:
                    origin.vector2IntValue = target == null ? default : target.vector2IntValue;
                    break;
                case SerializedPropertyType.Vector3Int:
                    origin.vector3IntValue = target == null ? default : target.vector3IntValue;
                    break;
                case SerializedPropertyType.RectInt:
                    origin.rectIntValue = target == null ? default : target.rectIntValue;
                    break;
                case SerializedPropertyType.BoundsInt:
                    origin.boundsIntValue = target == null ? default : target.boundsIntValue;
                    break;
                default:
                    Debug.LogError($"不支持的类型：{origin.propertyType}, displayName：{origin.displayName}");
                    break;
            }
        }

        private bool DrawStateField(SerializedProperty state, string typeName, Action drawBefore = null, Action afterDraw = null)
        {
            stateNameList.Clear();

            string foldoutFieldName = null;
            var fields = typeof(ControllerGroupState).GetFields();
            foreach (var field in fields)
            {
                var attri = field.GetCustomAttribute<UIControllerStateDrawerAttribute>();
                if (attri == null)
                    continue;

                if (attri.TypeName != typeName)
                    continue;

                switch (attri.FieldType)
                {
                    case UIControllerStateFieldType.Field:
                        stateNameList.Add((field.Name, attri.FieldName ?? field.Name));
                        break;
                    case UIControllerStateFieldType.Foldout:
                        foldoutFieldName = field.Name;
                        break;
                    default:
                        break;
                }
            }

            bool show = true;
            if (!string.IsNullOrEmpty(foldoutFieldName))
            {
                var foldout = state.FindPropertyRelative(foldoutFieldName);
                foldout.isExpanded = EditorGUILayout.Foldout(foldout.isExpanded, typeName, true);
                show = foldout.isExpanded;
            }

            if (show)
            {
                EditorGUI.indentLevel++;
                drawBefore?.Invoke();
                foreach (var item in stateNameList)
                {
                    EditorGUILayout.PropertyField(state.FindPropertyRelative(item.OriginName), new GUIContent(item.Tag));
                }
                afterDraw?.Invoke();
                EditorGUI.indentLevel--;
            }

            stateNameList.Clear();

            return show;
        }

        private void DeleteStateField(SerializedProperty state, string typeName)
        {
            var fields = typeof(ControllerGroupState).GetFields();
            foreach (var field in fields)
            {
                var attri = field.GetCustomAttribute<UIControllerStateDrawerAttribute>();
                if (attri == null)
                    continue;

                if (typeName != null && attri.TypeName != typeName)
                    continue;

                switch (attri.FieldType)
                {
                    case UIControllerStateFieldType.Field:
                        state.FindPropertyRelative(field.Name).boolValue = false;
                        break;
                }
            }
        }

        private void CopyToProperty(SerializedProperty origin, SerializedProperty target, Type objType, int index, params Type[] attriTypes)
        {
            var fields = objType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
            foreach (var item in fields)
            {
                if (attriTypes != null && index < attriTypes.Length && attriTypes[index] != null)
                {
                    if (item.GetCustomAttribute(attriTypes[index]) == null)
                        continue;
                }

                var originProperty = origin.FindPropertyRelative(item.Name);
                var targetProperty = target.FindPropertyRelative(item.Name);
                if (originProperty == null || targetProperty == null)
                    continue;

                if (index + 1 < attriTypes.Length)
                {
                    CopyToProperty(originProperty, targetProperty, item.FieldType, index + 1, attriTypes);
                }
                else
                {
                    UpdatePropertyValue(originProperty, targetProperty);
                }
            }
        }

        private void Apply()
        {
            serializedObject.ApplyModifiedProperties();
            //serializedObject.UpdateIfRequiredOrScript();
        }
    }
}
