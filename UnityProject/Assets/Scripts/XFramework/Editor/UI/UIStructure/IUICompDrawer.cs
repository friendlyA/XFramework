﻿using System;

namespace XFramework.Editor
{
    public interface IUICompDrawer
    {
        void Draw(UIComponent comp);

        Type GetCompType();
    }

    [UICompDrawerFlag]
    public abstract class UICompDrawer<T> : IUICompDrawer where T : UIComponent
    {
        public void Draw(UIComponent comp)
        {
            Run((T)comp);
        }

        public Type GetCompType()
        {
            return typeof(T);
        }

        protected abstract void Run(T comp);
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class UICompDrawerFlagAttribute : Attribute
    {

    }
}
