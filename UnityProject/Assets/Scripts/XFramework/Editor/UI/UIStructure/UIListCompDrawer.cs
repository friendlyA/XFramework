﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace XFramework.Editor
{
    internal class UIListCompDrawer : UICompDrawer<UIListComponent>
    {
        private static Dictionary<int, bool> foldoutDict = new Dictionary<int, bool>();

        protected override void Run(UIListComponent comp)
        {
            int index = -1;
            foreach (UI child in comp)
            {
                index++;
                var foldout = EditorGUILayout.Foldout(foldoutDict.Get(index), $"Element {index}", true);
                foldoutDict[index] = foldout;
                if (!foldout)
                    continue;

                EditorGUI.indentLevel++;

                if (child is IUID uid && uid.IsValid())
                    EditorGUILayout.TextField("Id", uid.Id.ToString());
                EditorGUILayout.TextField("Type", child.GetType().Name);
                EditorGUILayout.ObjectField("GameObject", child.GameObject, typeof(GameObject), true);

                EditorGUI.indentLevel--;
            }
        }
    }
}
