﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace XFramework.Editor
{
    [CustomEditor(typeof(UIStructure))]
    public class UIStructureEditor : UnityEditor.Editor
    {
        protected const string ChildrenName = "children";

        protected const string ComponentsName = "uiComponents";

        private static bool parentFoldout = false;

        private static bool childrenFoldout = false;

        private static bool componentFoldout = false;

        private static Dictionary<string, bool> foldoutStrDict = new Dictionary<string, bool>();

        private static Dictionary<int, bool> foldoutIntDict = new Dictionary<int, bool>();

        private static Dictionary<Type, IUICompDrawer> compDrawerDict = new Dictionary<Type, IUICompDrawer>();

        private void OnEnable()
        {
            InitCompMethod();
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var uiObject = (target as UIStructure).UIObject as UI;
            if (uiObject is null)
                return;

            DrawObject(uiObject);
            DrawParent(uiObject.Parent);
            DrawChildren(uiObject);
            DrawComponents(uiObject.UIComponents);
        }

        private static void InitCompMethod()
        {
            compDrawerDict.Clear();
            foreach (var type in typeof(UIOnLoadMethod).Assembly.GetTypes())
            {
                if (type.IsAbstract)
                    continue;

                if (type.GetCustomAttribute<UICompDrawerFlagAttribute>() == null)
                    continue;

                var obj = Activator.CreateInstance(type) as IUICompDrawer;
                if (obj == null)
                    continue;

                compDrawerDict.Add(obj.GetCompType(), obj);
            }
        }

        protected void DrawObject(UI ui)
        {
            EditorGUILayout.Space();
            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Type");
            EditorGUILayout.TextField(ui.GetType().Name, GUILayout.MaxWidth(250), GUILayout.MinWidth(100));
            GUILayout.EndHorizontal();
        }

        protected void DrawParent(UI parent)
        {
            if (parent != null)
            {
                EditorGUILayout.Space();
                parentFoldout = EditorGUILayout.Foldout(parentFoldout, "Parent", true);
                if (parentFoldout)
                {
                    EditorGUI.indentLevel++;
                    GUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(parent.Name);
                    EditorGUILayout.ObjectField(parent.GameObject, typeof(GameObject), this);
                    GUILayout.EndHorizontal();
                    EditorGUI.indentLevel--;
                }
            }
        }

        protected void DrawChildren(UI ui)
        {
            var children = ui.ChildrenSet;
            if (children != null && children.Count > 0)
            {
                EditorGUILayout.Space();
                childrenFoldout = EditorGUILayout.Foldout(childrenFoldout, "Children", true);
                if (childrenFoldout)
                {
                    EditorGUI.indentLevel++;
                    var index = -1;
                    foreach (var child in children)
                    {
                        index++;
                        var foldout = EditorGUILayout.Foldout(foldoutIntDict.Get(index), $"Element {index}", true);
                        foldoutIntDict[index] = foldout;
                        if (!foldout)
                            continue;

                        EditorGUI.indentLevel++;

                        EditorGUILayout.TextField(nameof(child.Name), child.Name);
                        EditorGUILayout.TextField(nameof(child.InChildren), child.InChildren.ToString());
                        EditorGUILayout.TextField("Type", child.GetType().Name);
                        EditorGUILayout.ObjectField("GameObject", child.GameObject, typeof(GameObject), true);

                        EditorGUI.indentLevel--;
                    }
                    EditorGUI.indentLevel--;
                }
            }
        }

        protected void DrawComponents(Dictionary<Type, UIComponent> components)
        {
            if (components != null && components.Count > 0)
            {
                EditorGUILayout.Space();
                componentFoldout = EditorGUILayout.Foldout(componentFoldout, "Components", true);
                if (componentFoldout)
                {
                    EditorGUI.indentLevel++;
                    foreach (var comp in components.Values)
                    {
                        var type = comp.GetType();
                        var name = type.ToString().Remove(0, type.Namespace.Length + 1);
                        var foldout = EditorGUILayout.Foldout(foldoutStrDict.Get(name), name, true);
                        foldoutStrDict[name] = foldout;
                        if (foldout)
                        {
                            EditorGUI.indentLevel++;
                            if (compDrawerDict.TryGetValue(type, out var obj))
                            {
                                obj.Draw(comp);
                            }
                            EditorGUI.indentLevel--;
                        }
                    }
                    EditorGUI.indentLevel--;
                    //foreach (var item in components)
                    //{
                    //    GUILayout.BeginHorizontal();
                    //    var type = item.Value.GetType();
                    //    EditorGUILayout.LabelField(type.ToString().Remove(0, type.Namespace.Length + 1));
                    //    GUILayout.EndHorizontal();
                    //}
                }
            }
        }
    }
}
