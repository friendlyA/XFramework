﻿using System;

namespace XFramework
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class ReplaceComponentAttribute : System.Attribute
    {
        public Type OriginType { get; }

        public int Order { get; set; }

        public ReplaceComponentAttribute(Type originType)
        {
            OriginType = originType;
        }
    }
}
