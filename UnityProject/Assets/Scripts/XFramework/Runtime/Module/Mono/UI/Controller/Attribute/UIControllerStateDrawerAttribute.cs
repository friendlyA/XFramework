﻿namespace XFramework
{
    [System.AttributeUsage(System.AttributeTargets.Field)]
    public class UIControllerStateDrawerAttribute : System.Attribute
    {
        public string TypeName { get; }

        public string FieldName { get; }

        public UIControllerStateFieldType FieldType { get; }

        public UIControllerStateDrawerAttribute(string typeName, UIControllerStateFieldType fieldType, string fieldName = null)
        {
            TypeName = typeName;
            FieldType = fieldType;
            FieldName = fieldName;
        }
    }

    public enum UIControllerStateFieldType
    {
        Field,
        Foldout,
    }
}
