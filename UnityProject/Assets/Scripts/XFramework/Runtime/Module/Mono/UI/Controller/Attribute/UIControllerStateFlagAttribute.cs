﻿using System;

namespace XFramework
{
    [AttributeUsage(AttributeTargets.Field)]
    public class UIControllerStateFlagAttribute : Attribute
    {
        public Type TargetType { get; }

        public UIControllerStateFlagAttribute(Type compType)
        {
            TargetType = compType;
        }
    }
}
