﻿using System;

namespace XFramework
{
    [System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class UIControllerStateValueAttribute : Attribute
    {
        public string FieldNames { get; }

        public bool Custom { get; }

        public UIControllerStateValueAttribute(string fieldNames) : this(fieldNames, false)
        {

        }

        public UIControllerStateValueAttribute(string fieldNames, bool custom)
        {
            FieldNames = fieldNames;
            Custom = custom;
        }
    }
}
