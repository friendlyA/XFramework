﻿using System.Collections.Generic;

namespace XFramework
{
    public interface IControllerGroupData
    {
        UIController Container { get; }

        int Index { get; }

        string Name { get; }

        int Count { get; }

        int SelectIndex { get; }

        void SetName(string name);

        void SetIndex(int index);

        void GetAllName(List<string> result);
    }
}
