﻿using UnityEngine;

namespace XFramework
{
    internal abstract class BehaviourCtl : IControllerUpdate
    {
        [HideInInspector]
        [UIControllerStateValue("m_Enabled")]
        public bool EnabledValue;

        public abstract Behaviour GetBehaviour(GameObject gameObject);

        public abstract bool GetEnabledState(ControllerGroupState state);

        protected virtual void UpdateState(ControllerGroupState state, Behaviour behaviour) { }

        public void UpdateState(ControllerGroupState state, GameObject gameObject)
        {
            var comp = GetBehaviour(gameObject);
            if (!comp)
                return;

            if (GetEnabledState(state))
                comp.enabled = EnabledValue;

            UpdateState(state, comp);
        }
    }
}
