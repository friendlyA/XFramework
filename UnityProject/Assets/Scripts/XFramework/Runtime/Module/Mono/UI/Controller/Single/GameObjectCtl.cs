﻿using UnityEngine;

namespace XFramework
{
    [System.Serializable]
    public partial class ControllerGroupState
    {
        #region GameObject

        [HideInInspector]
        [UIControllerStateDrawer(nameof(GameObject), UIControllerStateFieldType.Foldout)]
        public bool GameObjectFoldout;

        [UIControllerStateDrawer(nameof(GameObject), UIControllerStateFieldType.Field)]
        public bool Active;

        #endregion
    }

    [System.Serializable]
    internal class GameObjectCtl : IControllerUpdate
    {
        [HideInInspector]
        [UIControllerStateValue("m_IsActive")]
        public bool ActiveValue;

        public void UpdateState(ControllerGroupState state, GameObject gameObject)
        {
            if (state.Active)
            {
                if (gameObject.activeSelf != ActiveValue)
                    gameObject.SetActive(ActiveValue);
            }
        }
    }
}
