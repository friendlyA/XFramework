﻿using UnityEngine;
using UnityEngine.UI;

namespace XFramework
{
    internal abstract class GraphicCtl : BehaviourCtl
    {
        [HideInInspector]
        [UIControllerStateValue("m_Color")]
        public Color ColorValue;

        [HideInInspector]
        [UIControllerStateValue("m_Material")]
        public Material MaterialValue;

        public abstract bool GetColorState(ControllerGroupState state);

        public abstract bool GetMaterialState(ControllerGroupState state);

        protected override void UpdateState(ControllerGroupState state, Behaviour behaviour)
        {
            var graphic = behaviour as Graphic;
            if (!graphic)
                return;

            if (GetEnabledState(state))
                graphic.enabled = EnabledValue;

            if (GetColorState(state))
                graphic.color = ColorValue;

            if (GetMaterialState(state))
                graphic.material = MaterialValue;
        }
    }
}
