﻿using UnityEngine;

namespace XFramework
{
    internal interface IControllerUpdate
    {
        void UpdateState(ControllerGroupState state, GameObject gameObject);
    }
}
