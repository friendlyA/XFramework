﻿using UnityEngine;
using UnityEngine.UI;

namespace XFramework
{
    public partial class ControllerGroupState
    {
        #region Image

        [HideInInspector]
        [UIControllerStateDrawer(nameof(Image), UIControllerStateFieldType.Foldout)]
        public bool ImageFoldout;

        [UIControllerStateDrawer(nameof(Image), UIControllerStateFieldType.Field, "Enabled")]
        public bool ImageEnabled;

        [UIControllerStateDrawer(nameof(Image), UIControllerStateFieldType.Field, nameof(Color))]
        public bool ImageColor;

        [UIControllerStateDrawer(nameof(Image), UIControllerStateFieldType.Field, nameof(Material))]
        public bool ImageMaterial;

        [UIControllerStateDrawer(nameof(Image), UIControllerStateFieldType.Field, nameof(Sprite))]
        public bool ImageSprite;

        #endregion
    }

    [System.Serializable]
    internal class ImageCtl : GraphicCtl
    {
        [HideInInspector]
        [UIControllerStateValue("m_Sprite")]
        public Sprite SpriteValue;

        public override Behaviour GetBehaviour(GameObject gameObject)
        {
            return gameObject.GetComponent<Image>();
        }

        public override bool GetColorState(ControllerGroupState state)
        {
            return state.ImageColor;
        }

        public override bool GetEnabledState(ControllerGroupState state)
        {
            return state.ImageEnabled;
        }

        public override bool GetMaterialState(ControllerGroupState state)
        {
            return state.ImageMaterial;
        }

        protected override void UpdateState(ControllerGroupState state, Behaviour behaviour)
        {
            base.UpdateState(state, behaviour);

            var img = behaviour as Image;
            if (!img)
                return;

            if (state.ImageSprite)
                img.sprite = SpriteValue;
        }
    }
}
