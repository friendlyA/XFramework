﻿using UnityEngine;

namespace XFramework
{
    public partial class ControllerGroupState
    {
        #region RectTransform

        [HideInInspector]
        [UIControllerStateDrawer(nameof(RectTransform), UIControllerStateFieldType.Foldout)]
        public bool RectTransformFoldout;

        [UIControllerStateDrawer(nameof(RectTransform), UIControllerStateFieldType.Field, "Anchored Position")]
        public bool AnchoredPosition;

        [UIControllerStateDrawer(nameof(RectTransform), UIControllerStateFieldType.Field)]
        public bool Width;

        [UIControllerStateDrawer(nameof(RectTransform), UIControllerStateFieldType.Field)]
        public bool Height;

        [UIControllerStateDrawer(nameof(RectTransform), UIControllerStateFieldType.Field)]
        public bool Anchors;

        [UIControllerStateDrawer(nameof(RectTransform), UIControllerStateFieldType.Field)]
        public bool Pivot;

        #endregion
    }

    [System.Serializable]
    internal class RectTransformCtl : IControllerUpdate
    {
        [HideInInspector]
        [UIControllerStateValue("m_AnchoredPosition")]
        public Vector2 AnchoredPositionValue;

        [HideInInspector]
        [UIControllerStateValue("m_SizeDelta.x")]
        public float WidthValue;

        [HideInInspector]
        [UIControllerStateValue("m_SizeDelta.y")]
        public float HeightValue;

        [HideInInspector]
        [UIControllerStateValue("m_AnchorMin")]
        public Vector2 AnchorsMinValue;

        [HideInInspector]
        [UIControllerStateValue("m_AnchorMax")]
        public Vector2 AnchorsMaxValue;

        [HideInInspector]
        [UIControllerStateValue("m_Pivot")]
        public Vector2 PivotValue;

        public void UpdateState(ControllerGroupState state, GameObject gameObject)
        {
            var trans = gameObject.transform as RectTransform;
            if (!trans)
                return;

            float width = state.Width ? WidthValue : trans.rect.width;
            float height = state.Height ? HeightValue : trans.rect.height;

            if (state.AnchoredPosition)
                trans.anchoredPosition = AnchoredPositionValue;

            if (state.Width || state.Height)
                trans.sizeDelta = new Vector2(width, height);

            if (state.Anchors)
            {
                trans.anchorMin = AnchorsMinValue;
                trans.anchorMax = AnchorsMaxValue;
            }

            if (state.Pivot)
            {
                trans.pivot = PivotValue;
            }
        }
    }
}
