﻿using UnityEngine;
using UnityEngine.UI;

namespace XFramework
{
    public partial class ControllerGroupState
    {
        #region Text

        [HideInInspector]
        [UIControllerStateDrawer(nameof(Text), UIControllerStateFieldType.Foldout)]
        public bool TextFoldout;

        [UIControllerStateDrawer(nameof(Text), UIControllerStateFieldType.Field, "Enabled")]
        public bool TextEnabled;

        [UIControllerStateDrawer(nameof(Text), UIControllerStateFieldType.Field, nameof(Color))]
        public bool TextColor;

        [UIControllerStateDrawer(nameof(Text), UIControllerStateFieldType.Field, nameof(Material))]
        public bool TextMaterial;

        [UIControllerStateDrawer(nameof(Text), UIControllerStateFieldType.Field, "Text")]
        public bool TextContent;

        [UIControllerStateDrawer(nameof(Text), UIControllerStateFieldType.Field, "Font Style")]
        public bool TextFontStyle;

        [UIControllerStateDrawer(nameof(Text), UIControllerStateFieldType.Field, "Font Size")]
        public bool TextFontSize;

        [UIControllerStateDrawer(nameof(Text), UIControllerStateFieldType.Field, "Line Spacing")]
        public bool TextLineSpacing;

        [UIControllerStateDrawer(nameof(Text), UIControllerStateFieldType.Field, "Rich Text")]
        public bool TextRich;

        [UIControllerStateDrawer(nameof(Text), UIControllerStateFieldType.Field, "Alignment")]
        public bool TextAlignment;

        #endregion
    }

    [System.Serializable]
    internal class TextCtl : GraphicCtl
    {
        [HideInInspector]
        [UIControllerStateValue("m_Text")]
        public string TextValue;

        [HideInInspector]
        [UIControllerStateValue("m_FontData.m_FontStyle")]
        public FontStyle FontStyleValue;

        [HideInInspector]
        [UIControllerStateValue("m_FontData.m_FontSize")]
        public int FontSizeValue;

        [HideInInspector]
        [UIControllerStateValue("m_FontData.m_LineSpacing")]
        public float LineSpacingValue;

        [HideInInspector]
        [UIControllerStateValue("m_FontData.m_RichText")]
        public bool RichValue;

        [HideInInspector]
        [UIControllerStateValue("m_FontData.m_Alignment")]
        public TextAnchor AlignmentValue;

        public override Behaviour GetBehaviour(GameObject gameObject)
        {
            return gameObject.GetComponent<Text>();
        }

        public override bool GetColorState(ControllerGroupState state)
        {
            return state.TextColor;
        }

        public override bool GetEnabledState(ControllerGroupState state)
        {
            return state.TextEnabled;
        }

        public override bool GetMaterialState(ControllerGroupState state)
        {
            return state.TextMaterial;
        }

        protected override void UpdateState(ControllerGroupState state, Behaviour behaviour)
        {
            base.UpdateState(state, behaviour);

            var txt = behaviour as Text;
            if (!txt)
                return;

            if (state.TextContent)
                txt.text = TextValue;

            if (state.TextFontStyle)
                txt.fontStyle = FontStyleValue;

            if (state.TextFontSize)
                txt.fontSize = FontSizeValue;

            if (state.TextLineSpacing)
                txt.lineSpacing = LineSpacingValue;

            if (state.TextRich)
                txt.supportRichText = RichValue;

            if (state.TextAlignment)
                txt.alignment = AlignmentValue;
        }
    }
}
