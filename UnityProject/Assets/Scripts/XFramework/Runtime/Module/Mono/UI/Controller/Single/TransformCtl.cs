﻿using UnityEngine;

namespace XFramework
{
    public partial class ControllerGroupState
    {
        #region Transform

        [HideInInspector]
        [UIControllerStateDrawer(nameof(Transform), UIControllerStateFieldType.Foldout)]
        public bool TransformFoldout;

        [UIControllerStateDrawer(nameof(Transform), UIControllerStateFieldType.Field, "Local Position")]
        public bool LocalPosition;

        [UIControllerStateDrawer(nameof(Transform), UIControllerStateFieldType.Field, "Local Rotation")]
        public bool LocalRotation;

        [UIControllerStateDrawer(nameof(Transform), UIControllerStateFieldType.Field, "Local Scale")]
        public bool LocalScale;

        #endregion
    }

    [System.Serializable]
    internal class TransformCtl : IControllerUpdate
    {
        [HideInInspector]
        [UIControllerStateValue("m_LocalPosition")]
        public Vector3 LocalPositionValue;

        [HideInInspector]
        [UIControllerStateValue("m_LocalRotation")]
        public Quaternion LocalRotationValue;

        [HideInInspector]
        [UIControllerStateValue("m_LocalScale")]
        public Vector3 LocalScaleValue;

        public void UpdateState(ControllerGroupState state, GameObject gameObject)
        {
            var trans = gameObject.transform;

            if (state.LocalPosition)
                trans.localPosition = LocalPositionValue;

            if (state.LocalRotation)
                trans.rotation = LocalRotationValue;

            if (state.LocalScale)
                trans.localScale = LocalScaleValue;
        }
    }
}
