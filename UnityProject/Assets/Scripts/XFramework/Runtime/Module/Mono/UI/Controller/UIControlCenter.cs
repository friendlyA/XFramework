﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace XFramework
{
    [DisallowMultipleComponent]
    public class UIControlCenter : MonoBehaviour, ISerializationCallbackReceiver
    {
        [System.Serializable]
        public class ChangeIndexEvent : UnityEvent
        {

        }

        [System.Serializable]
        class ControllerIndex
        {
            public int Index;

            public int ChildIndex;
        }

        [System.Serializable]
        class Controller
        {
            public UIController Ctl;

            public List<ControllerIndex> ControllerIndexList = new List<ControllerIndex>();

            public void UpdateState(bool update)
            {
                if (!Ctl)
                    return;

                foreach (var item in ControllerIndexList)
                {
                    if (update && Ctl.GetSelectIndex(item.Index) == item.ChildIndex)
                        Ctl.UpdateState(item.Index);
                    else
                        Ctl.SetIndex(item.Index, item.ChildIndex);
                }

#if UNITY_EDITOR
                if (!Application.isPlaying)
                {
                    UnityEditor.EditorUtility.SetDirty(Ctl);
                }
#endif
            }

            public bool IsValid()
            {
                if (!Ctl)
                    return false;

                foreach (var item in ControllerIndexList)
                {
                    if (!Ctl.IsValidIndex(item.Index, false))
                        return false;

                    if (!Ctl.GetControllerGroupAt(item.Index).IsValidIndex(item.ChildIndex, false))
                        return false;
                }

                return true;
            }

            public void OnBeforeSerialize()
            {
                if (!Ctl)
                    return;

                foreach (var item in ControllerIndexList)
                {
                    if (!Ctl.IsValidIndex(item.Index, false))
                        item.Index = 0;

                    if (!Ctl.GetControllerGroupAt(item.Index).IsValidIndex(item.ChildIndex, false))
                        item.ChildIndex = 0;
                }
            }
        }

        [System.Serializable]
        class ControlCenter
        {
            public UIControlCenter CtlCenter;

            public List<int> ControlIndexList = new List<int>() { 0 };

            public void UpdateState(bool update)
            {
                if (!CtlCenter)
                    return;

                foreach (var index in ControlIndexList)
                {
                    if (update && CtlCenter.GetSelectIndex() == index)
                        CtlCenter.UpdateState();
                    else
                        CtlCenter.SetIndex(index);
                }

#if UNITY_EDITOR
                if (!Application.isPlaying)
                {
                    UnityEditor.EditorUtility.SetDirty(CtlCenter);
                }
#endif
            }

            public bool IsValid()
            {
                if (!CtlCenter)
                    return false;

                foreach (var index in ControlIndexList)
                {
                    if (!CtlCenter.IsValidIndex(index, false))
                        return false;
                }

                return true;
            }

            public void OnBeforeSerialize()
            {
                if (!CtlCenter)
                    return;

                for (int i = 0; i < ControlIndexList.Count; i++)
                {
                    if (!CtlCenter.IsValidIndex(ControlIndexList[i], false))
                        ControlIndexList[i] = 0;
                }
            }
        }

        [System.Serializable]
        class ControlData
        {
            [System.NonSerialized]
            public int Index;

            public string Name;

            public List<Controller> ControllerList = new List<Controller>();

            public List<ControlCenter> ControlCenterList = new List<ControlCenter>();

            public void UpdateState(bool update)
            {
                foreach (var item in ControllerList)
                {
                    item.UpdateState(update);
                }

                foreach (var item in ControlCenterList)
                {
                    item.UpdateState(update);
                }
            }

            public void OnBeforeSerialize()
            {
                if (!string.IsNullOrEmpty(Name))
                    Name = Name.Trim();

                foreach (var item in ControllerList)
                {
                    item.OnBeforeSerialize();
                }

                foreach (var item in ControlCenterList)
                {
                    item.OnBeforeSerialize();
                }
            }
        }

        [SerializeField]
        private int selectIndex;

        [SerializeField]
        private List<ControlData> controlDatas = new List<ControlData>() { new ControlData() };

        [SerializeField]
        private ChangeIndexEvent m_OnChanged = new ChangeIndexEvent();

        private Dictionary<string, ControlData> controlDataDict = new Dictionary<string, ControlData>();

        private ControlData SelectControlData => IsValidIndex(selectIndex, false) ? controlDatas[selectIndex] : null;

        public int Count => controlDatas.Count;

        public ChangeIndexEvent OnChanged => m_OnChanged;

        private void Start()
        {
            if (!IsValidIndex(selectIndex, false))
                selectIndex = 0;

            if (!IsValidIndex(selectIndex, false))
                return;

            UpdateState();
        }

        /// <summary>
        /// 索引是否有效
        /// </summary>
        /// <param name="index"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        private bool IsValidIndex(int index, bool error)
        {
            var valid = index >= 0 && index < Count;
            if (error && !valid)
            {
                throw new System.IndexOutOfRangeException("index");
            }

            return valid;
        }

        private ControlData GetControlData(string name)
        {
            return controlDataDict.Get(name);
        }

        private ControlData GetControlDataAt(int index)
        {
            if (!IsValidIndex(index, true))
                return null;

            return controlDatas[index];
        }

        private void OnChangedIndex()
        {
            m_OnChanged.Invoke();
        }

        public void SetIndex(int ctlIndex)
        {
            if (selectIndex == ctlIndex)
                return;

            var ctlData = GetControlDataAt(ctlIndex);
            if (ctlData == null)
                return;

            selectIndex = ctlIndex;
            ctlData.UpdateState(false);

            OnChangedIndex();
        }

        public void SetName(string ctlName)
        {
            var ctlData = GetControlData(ctlName);
            if (ctlData == null)
                return;

            SetIndex(ctlData.Index);
        }

        public void UpdateState()
        {
            SelectControlData?.UpdateState(true);
        }

        public void GetAllName(List<string> result)
        {
            result.AddRange(controlDataDict.Keys);
        }

        public int GetSelectIndex()
        {
            return selectIndex;
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            if (!IsValidIndex(selectIndex, false))
                selectIndex = 0;

            foreach (var item in controlDatas)
            {
                item.OnBeforeSerialize();
            }
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            controlDataDict.Clear();
            for (int i = 0; i < controlDatas.Count; i++)
            {
                var item = controlDatas[i];
                item.Index = i;

                string name = item.Name?.Trim();
                if (!string.IsNullOrEmpty(item.Name) && !controlDataDict.ContainsKey(name))
                {
                    controlDataDict.Add(name, item);
                }
            }
        }
    }
}
