﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace XFramework
{
    [DisallowMultipleComponent]
    public class UIController : MonoBehaviour, ISerializationCallbackReceiver
    {
        [System.Serializable]
        public class ChangeIndexEvent : UnityEvent<int>
        {

        }

        [System.Serializable]
        internal class Controller : IControllerUpdate
        {
            [System.NonSerialized]
            public int Index;

            public string Name;

            [UIControllerStateFlag(typeof(GameObject))]
            public GameObjectCtl GameObject = new GameObjectCtl();

            [UIControllerStateFlag(typeof(Transform))]
            public TransformCtl Transform = new TransformCtl();

            [UIControllerStateFlag(typeof(RectTransform))]
            public RectTransformCtl RectTransform = new RectTransformCtl();

            [UIControllerStateFlag(typeof(Image))]
            public ImageCtl Image = new ImageCtl();

            [UIControllerStateFlag(typeof(Text))]
            public TextCtl Text = new TextCtl();

            public IEnumerable<IControllerUpdate> GetEnumerable()
            {
                yield return GameObject;
                yield return Transform;
                yield return RectTransform;
                yield return Image;
                yield return Text;
            }

            public void UpdateState(ControllerGroupState state, GameObject gameObject)
            {
                foreach (var item in GetEnumerable())
                {
                    item.UpdateState(state, gameObject);
                }
            }
        }        

        [System.Serializable]
        internal class ControllerGroup : IControllerGroupData
        {
            [System.NonSerialized]
            public int Index;

            [System.NonSerialized]
            public UIController Container;

            public string Name;

            public int SelectIndex;

            public ControllerGroupState State = new ControllerGroupState();

            public List<Controller> Controllers = new List<Controller>() { new Controller() };

            public int Count => Controllers.Count;

            public Controller SelectCtl => !IsValidIndex(SelectIndex, false) ? null : GetControllerAt(SelectIndex);

            UIController IControllerGroupData.Container => Container;

            int IControllerGroupData.Index => Index;

            int IControllerGroupData.SelectIndex => SelectIndex;

            string IControllerGroupData.Name => Name;

            [System.NonSerialized]
            public Dictionary<string, Controller> ControllerDict = new Dictionary<string, Controller>();

            public void OnAfterDeserialize()
            {
                ControllerDict.Clear();
                for (int i = 0; i < Controllers.Count; i++)
                {
                    var item = Controllers[i];
                    item.Index = i;

                    var name = item.Name?.Trim();
                    if (!string.IsNullOrEmpty(name) && !ControllerDict.ContainsKey(name))
                    {
                        ControllerDict.Add(name, item);
                    }
                }
            }

            /// <summary>
            /// 索引是否有效
            /// </summary>
            /// <param name="index"></param>
            /// <param name="error"></param>
            /// <returns></returns>
            public bool IsValidIndex(int index, bool error)
            {
                var valid = index >= 0 && index < Count;
                if (error && !valid)
                {
                    throw new System.IndexOutOfRangeException("index");
                }

                return valid;
            }

            public Controller GetController(string name)
            {
                return ControllerDict.Get(name);
            }

            public Controller GetControllerAt(int index)
            {
                if (!IsValidIndex(index, true))
                    return null;

                return Controllers[index];
            }

            public void SetName(string name)
            {
                var controller = this.Container;
                if (!controller || name.IsNullOrEmpty())
                    return;

                var targetCtl = GetController(name);
                if (targetCtl == null)
                    return;

                SetIndex(targetCtl.Index);
            }

            public void SetIndex(int index)
            {
                var controller = this.Container;
                if (!controller || SelectIndex == index)
                    return;

                var ctl = GetControllerAt(index);
                if (ctl == null)
                    return;

                SelectIndex = index;
                ctl.UpdateState(State, controller.gameObject);

                controller.OnChangedIndex(Index);
            }

            public void UpdateState()
            {
                if (!Container)
                    return;

                SelectCtl?.UpdateState(State, Container.gameObject);
            }

            public void GetAllName(List<string> result)
            {
                result.AddRange(ControllerDict.Keys);
            }
        }

        public static readonly Type ControllerType = typeof(Controller);

        public static readonly Type ControllerGroupType = typeof(ControllerGroup);

        [SerializeField]
        private ChangeIndexEvent m_OnChanged = new ChangeIndexEvent();

        [SerializeField]
        private List<ControllerGroup> controllerList = new List<ControllerGroup>() { new ControllerGroup() };

        private Dictionary<string, ControllerGroup> controllerDict = new Dictionary<string, ControllerGroup>();

        public int Count => controllerList.Count;

        public ChangeIndexEvent OnChanged => m_OnChanged;

        internal ControllerGroup GetControllerGroup(string name)
        {
            return controllerDict.Get(name);
        }

        internal ControllerGroup GetControllerGroupAt(int index)
        {
            if (!IsValidIndex(index, true))
                return null;

            return controllerList[index];
        }

        /// <summary>
        /// 索引是否有效
        /// </summary>
        /// <param name="index"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        internal bool IsValidIndex(int index, bool error)
        {
            var valid = index >= 0 && index < Count;
            if (error && !valid)
            {
                throw new System.IndexOutOfRangeException("index");
            }

            return valid;
        }

        private void OnChangedIndex(int index)
        {
            m_OnChanged.Invoke(index);
        }

        public IControllerGroupData GetCtlGroup(string name)
        {
            return GetControllerGroup(name);
        }

        public IControllerGroupData GetCtlGroupAt(int index)
        {
            return GetControllerGroupAt(index);
        }

        public void SetIndex(string ctlName, int childIndex)
        {
            var group = GetControllerGroup(ctlName);
            if (group == null)
                return;

            group.SetIndex(childIndex);
        }

        public void SetIndex(int ctlIndex, int childIndex)
        {
            var group = GetControllerGroupAt(ctlIndex);
            if (group == null)
                return;

            group.SetIndex(childIndex);
        }

        public void SetName(string ctlName, string childName)
        {
            var group = GetControllerGroup(ctlName);
            if (group == null)
                return;

            group.SetName(childName);
        }

        public void SetName(int ctlIndex, string childName)
        {
            var group = GetControllerGroupAt(ctlIndex);
            if (group == null)
                return;

            group.SetName(childName);
        }

        public void UpdateState(string ctlName)
        {
            var group = GetControllerGroup(ctlName);
            if (group == null)
                return;

            group.UpdateState();
        }

        public void UpdateState(int ctlIndex)
        {
            var group = GetControllerGroupAt(ctlIndex);
            if (group == null)
                return;

            group.UpdateState();
        }

        public void GetAllName(List<string> result)
        {
            result.AddRange(controllerDict.Keys);
        }

        public int GetSelectIndex(string ctlName)
        {
            var group = GetControllerGroup(ctlName);
            if (group == null)
                return -1;

            return group.SelectIndex;
        }

        public int GetSelectIndex(int ctlIndex)
        {
            var group = GetControllerGroupAt(ctlIndex);
            if (group == null)
                return -1;

            return group.SelectIndex;
        }

        public int GetCtlGroupCount(string ctlName)
        {
            var group = GetControllerGroup(ctlName);
            if (group == null)
                return 0;

            return group.Count;
        }

        public int GetCtlGroupCount(int ctlIndex)
        {
            var group = GetControllerGroupAt(ctlIndex);
            if (group == null)
                return 0;

            return group.Count;
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            controllerDict.Clear();
            var index = 0;
            foreach (var item in controllerList)
            {
                item.Index = index++;
                item.Container = this;
                item.OnAfterDeserialize();

                var name = item.Name?.Trim();
                if (!string.IsNullOrEmpty(name) && !controllerDict.ContainsKey(name))
                {
                    controllerDict.Add(name, item);
                }
            }
        }
    }
}
