﻿namespace XFramework
{
    public class UIControlCenterComponent : UComponent<UIControlCenter>
    {
        public UIControlCenter.ChangeIndexEvent OnChanged => Get().OnChanged;

        protected override void Destroy()
        {
            OnChanged.RemoveAllListeners();
            base.Destroy();
        }
    }

    public static class UIControlCenterExtensions
    {
        public static UIControlCenterComponent GetUIControlCenter(this UI self)
        {
            return self.TakeComponent<UIControlCenterComponent, UIControlCenter>(true);
        }

        public static UIControlCenterComponent GetUIControlCenter(this UI self, string key)
        {
            UI ui = self.GetFromKeyOrPath(key);
            return ui?.GetUIControlCenter();
        }
    }
}
