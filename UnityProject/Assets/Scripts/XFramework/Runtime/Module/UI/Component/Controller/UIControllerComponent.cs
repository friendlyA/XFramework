﻿namespace XFramework
{
    public class UIControllerComponent : UComponent<UIController>
    {
        public UIController.ChangeIndexEvent OnChanged => Get().OnChanged;

        public int Count => Get().Count;

        public IControllerGroupData GetCtlGroup(string name)
        {
            return Get().GetControllerGroup(name);
        }

        public IControllerGroupData GetCtlGroupAt(int index)
        {
            return Get().GetControllerGroupAt(index);
        }

        protected override void Destroy()
        {
            OnChanged.RemoveAllListeners();
            base.Destroy();
        }
    }

    public static class UIControllerExtensions
    {
        public static UIControllerComponent GetUIController(this UI self)
        {
            return self.TakeComponent<UIControllerComponent, UIController>(true);
        }

        public static UIControllerComponent GetUIController(this UI self, string key)
        {
            UI ui = self.GetFromKeyOrPath(key);
            return ui?.GetUIController();
        }
    }
}
