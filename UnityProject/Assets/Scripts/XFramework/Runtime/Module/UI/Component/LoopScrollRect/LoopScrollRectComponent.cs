﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace XFramework
{
    [UIComponentFlag]
    public abstract class LoopScrollRectComponent : UIBehaviourComponent<LoopScrollRect>, ILoopScrollRectPrefabKey, LoopScrollDataSource, LoopScrollPrefabSource
    {
        protected const string PoolName = "__Pool__";

        private string key;

        public int TotalCount => this.Get().totalCount;

        public UI Content { get; private set; }

        public UIListComponent ListContent { get; private set; }

        public UnityEvent<Vector2> OnValueChanged => this.Get().onValueChanged;

        protected Queue<GameObject> gameObjPool = new Queue<GameObject>();

        protected UI uiPool;

        protected override void EndInitialize()
        {
            base.EndInitialize();
            this.SetDefaultValue();
        }

        protected override void SetParentAfter()
        {
            base.SetParentAfter();

            if (this.Content is null)
            {
                if (this.Get() != null && this.Get().content != null)
                {
                    Content = this.parent.AddChild("Content", this.Get().content.gameObject, true);
                }
            }
            else
            {
                this.parent.AddChild(this.Content);
            }

            ListContent = Content.GetList();

            if (uiPool is null)
            {
                var transform = this.parent.GameObject.transform;
                var poolObj = transform.Find(PoolName)?.gameObject;
                if (!poolObj)
                {
                    poolObj = new GameObject(PoolName, typeof(RectTransform));
                    transform.AddChildAt(poolObj.transform, 0, false);
                    poolObj.transform.SetLocalPosition(Vector3.zero);
                }

                uiPool = this.parent.AddChild(PoolName, poolObj, true);
            }

            uiPool.SetActive(false);
        }

        protected override void Destroy()
        {
            this.OnValueChanged.RemoveAllListeners();
            this.key = null;
            this.Get().ClearCells();
            this.SetDataSource(null);
            this.SetPrefabSource(null);
            this.ListContent = null;
            this.Content = null;
            this.gameObjPool.Clear();
            this.uiPool = null;
            //this.SetTotalCount(0);
            base.Destroy();
        }

        public void SetPrefabSource(LoopScrollPrefabSource prefabSource)
        {
            this.Get().prefabSource = prefabSource;
        }

        public void SetDataSource(LoopScrollDataSource dataSource)
        {
            this.Get().dataSource = dataSource;
        }

        public void SetDefaultValue()
        {
            this.SetDataSource(this);
            this.SetPrefabSource(this);
        }

        public void SetKey(string key)
        {
            this.key = key;
        }

        public void SetTotalCount(int totalCount)
        {
            this.Get().totalCount = totalCount;
        }

        public void RefillCells(int startItem = 0, bool fillViewRect = false, float contentOffset = 0)
        {
            this.Get().RefillCells(startItem, fillViewRect, contentOffset);
        }

        public void RefillCellsFromEnd(int endItem = 0, bool alignStart = false)
        {
            this.Get().RefillCellsFromEnd(endItem, alignStart);
        }

        public void RefreshCells()
        {
            this.Get().RefreshCells();
        }

        public void SetVerticalNormalizedPosition(float value)
        {
            this.Get().verticalNormalizedPosition = value;
        }

        public void SetHorizontalNormalizedPosition(float value)
        {
            this.Get().horizontalNormalizedPosition = value;
        }

        protected abstract void ProvideData(Transform transform, int index);

        protected abstract void ReturnObject(Transform transform);

        #region Interface

        public string Key => key;

        void LoopScrollDataSource.ProvideData(Transform transform, int idx)
        {
            this.ProvideData(transform, idx);
        }

        GameObject LoopScrollPrefabSource.GetObject(int index)
        {
            if (this.Key.IsNullOrEmpty())
                throw new ArgumentNullException("LoopScrollPrefabSource.GetObject获取对象失败，未设置Key");

            if (gameObjPool.Count > 0)
            {
                var obj = gameObjPool.Dequeue();
                obj.SetViewActive(true);
                return obj;
            }

            return ResourcesManager.Instantiate(this, this.Key, this.Get().content, true);
        }

        void LoopScrollPrefabSource.ReturnObject(Transform trans)
        {
            this.ReturnObject(trans);
            GameObject obj = trans.gameObject;
            gameObjPool.Enqueue(obj);
            trans.SetParent(uiPool.GameObject.transform, false);
        }

        #endregion
    }

    public class LoopScrollRectComponent<T> : LoopScrollRectComponent where T : UI, new()
    {
        protected ILoopScrollRectProvide<T> provideData;

        protected Dictionary<int, T> children = new Dictionary<int, T>();

        protected Dictionary<int, T> childrenPool = new Dictionary<int, T>();

        protected Dictionary<int, string> childrenName = new Dictionary<int, string>();

        protected override void Destroy()
        {
            base.Destroy();
            this.children.Clear();
            this.childrenPool.Clear();
            this.childrenName.Clear();
            this.provideData = null;
        }

        public void SetProvideData(ILoopScrollRectProvide<T> provideData)
        {
            this.provideData = provideData;
        }

        public void SetProvideData(string key, ILoopScrollRectProvide<T> provideData)
        {
            this.SetKey(key);
            this.SetProvideData(provideData);
        }

        protected void RemoveChild(int instanceId)
        {
            if (children.TryRemove(instanceId, out var child))
            {
                if (child is IUID o)
                    o.SetId(0);

                childrenPool.Add(instanceId, child);
                uiPool.AddChild(child);
            }
        }

        protected void AddChild(int instanceId, T child)
        {
            if (!children.ContainsKey(instanceId))
            {
                childrenPool.Remove(instanceId);
                children.Add(instanceId, child);
                ListContent.AddChild(child, false);
            }
        }

        #region override

        protected override void ProvideData(Transform transform, int idx)
        {
            GameObject obj = transform.gameObject;
            int instanceId = obj.GetInstanceID();
            this.RemoveChild(instanceId);

            T child = childrenPool.Get(instanceId);

            string name = childrenName.Get(instanceId);
            if (name.IsNullOrEmpty())
            {
                name = instanceId.ToString();
                childrenName.Add(instanceId, name);
            }

            if (child is null)
            {
                child = UI.Create<T>(name, obj, true);
                ObjectHelper.Awake(child);
            }

            this.AddChild(instanceId, child);
            this.provideData.ProvideData(child, idx);
        }

        protected override void ReturnObject(Transform trans)
        {
            GameObject obj = trans.gameObject;
            int instanceId = obj.GetInstanceID();
            this.RemoveChild(instanceId);
        }

        #endregion
    }

    public static class LoopScrollRectExtensions
    {
        public static LoopScrollRectComponent<T> GetLoopScrollRect<T>(this ILoopScrollRectProvide<T> self) where T : UI, new()
        {
            if (!(self is UI ui))
                return null;

            return self.GetLoopScrollRect(ui);
        }

        public static LoopScrollRectComponent<T> GetLoopScrollRect<T>(this ILoopScrollRectProvide<T> self, UI ui) where T : UI, new()
        {
            var comp = ui.GetLoopScrollRect<T>();
            return comp;
        }

        public static LoopScrollRectComponent<T> GetLoopScrollRect<T>(this UI self) where T : UI, new()
        {
            var comp = self.GetUIComponent<LoopScrollRectComponent<T>>();
            if (comp != null)
                return comp;

            var scrollRect = self.GetComponent<LoopScrollRect>();
            if (!scrollRect)
                return null;

            comp = ObjectFactory.Create<LoopScrollRectComponent<T>, UnityEngine.Component>(scrollRect, true);
            if (self is ILoopScrollRectProvide<T> provide)
                comp.SetProvideData(provide);

            self.AddUIComponent(comp);

            return comp;
        }

        public static LoopScrollRectComponent<T> GetLoopScrollRect<T>(this UI self, string key) where T : UI, new()
        {
            var ui = self.GetFromKeyOrPath(key);
            return ui?.GetLoopScrollRect<T>();
        }
    }
}
