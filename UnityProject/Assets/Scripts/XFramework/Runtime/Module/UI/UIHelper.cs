using UnityEngine;

namespace XFramework
{
    public static class UIHelper
    {
        #region Create Sync

        /// <summary>
        /// 创建一个不受UIManager管理的UI，通常是作为某UI的子UI(公共UI)
        /// </summary>
        /// <param name="uiType"></param>
        /// <param name="parentObj"></param>
        /// <param name="awake">立即初始化</param>
        /// <returns></returns>
        public static UI Create(string uiType, Transform parentObj, bool awake = true)
        {
            UI ui = Common.Instance.Get<UIManager>()?.Create(uiType, parentObj, awake);
            return ui;
        }

        /// <summary>
        /// 创建一个不受UIManager管理的UI，通常是作为某UI的子UI，带一个初始化参数，如果需要更多的参数，可以自行扩展或者使用struct
        /// </summary>
        /// <typeparam name="P1"></typeparam>
        /// <param name="uiType"></param>
        /// <param name="p1"></param>
        /// <param name="parentObj"></param>
        /// <returns></returns>
        public static UI Create<P1>(string uiType, P1 p1, Transform parentObj)
        {
            UI ui = Common.Instance.Get<UIManager>()?.Create(uiType, p1, parentObj);
            return ui;
        }

        /// <summary>
        /// 创建一个受UIManager管理的UI，通常是作为独立UI
        /// </summary>
        /// <param name="uiType"></param>
        /// <param name="layer"></param>
        /// <returns></returns>
        public static UI Create(string uiType, UILayer layer)
        {
            UI ui = Common.Instance.Get<UIManager>()?.Create(uiType, layer, true);
            return ui;
        }

        /// <summary>
		/// 创建一个受UIManager管理的UI，通常是作为独立UI，带一个初始化参数，如果需要更多的参数，可以自行扩展或者使用struct
        /// </summary>
        /// <typeparam name="P1"></typeparam>
        /// <param name="uiType"></param>
        /// <param name="p1"></param>
        /// <param name="layer"></param>
        /// <returns></returns>
        public static UI Create<P1>(string uiType, P1 p1, UILayer layer)
        {
            UI ui = Common.Instance.Get<UIManager>()?.Create(uiType, p1, layer);
            return ui;
        }

        /// <summary>
        /// 创建一个受UIManager管理的UI，通常是作为独立UI
        /// <para>层级为默认配置层级</para>
        /// </summary>
        /// <param name="uiType"></param>
        /// <returns></returns>
        public static UI Create(string uiType)
        {
            UI ui = Common.Instance.Get<UIManager>()?.Create(uiType);
            return ui;
        }

        /// <summary>
        /// 创建一个受UIManager管理的UI，通常是作为独立UI，带一个初始化参数，如果需要更多的参数，可以自行扩展或者使用struct
        /// <para>层级为默认配置层级</para>
        /// </summary>
        /// <typeparam name="P1"></typeparam>
        /// <param name="uiType"></param>
        /// <param name="p1"></param>
        /// <returns></returns>
        public static UI Create<P1>(string uiType, P1 p1)
        {
            UI ui = Common.Instance.Get<UIManager>()?.Create(uiType, p1);
            return ui;
        }

        #endregion

        #region Create Async

        /// <summary>
        /// 异步创建一个不受UIManager管理的UI，通常是作为某UI的子UI(公共UI)
        /// </summary>
        /// <param name="source"></param>
        /// <param name="uiType"></param>
        /// <param name="parentObj"></param>
        /// <param name="awake">立即初始化</param>
        /// <returns></returns>
        public static async XFTask<UI> CreateAsync(XObject source, string uiType, Transform parentObj, bool awake = true)
        {
            var tag = source.TagId;
            UI ui = await Common.Instance.Get<UIManager>()?.CreateAsync(uiType, parentObj, awake);
            if (tag != source.TagId)    // 异步方法返回后会继续执行后面的逻辑，但是可能在返回前此类就释放掉了，为了避免这种情况，要用tag判断是否被释放过
            {
                ui?.Dispose();
                return null;
            }
                
            return ui;
        }

        /// <summary>
        /// 异步创建一个不受UIManager管理的UI，通常是作为某UI的子UI，带一个初始化参数，如果需要更多的参数，可以自行扩展或者使用struct
        /// </summary>
        /// <typeparam name="P1"></typeparam>
        /// <param name="source"></param>
        /// <param name="uiType"></param>
        /// <param name="p1"></param>
        /// <param name="parentObj"></param>
        /// <returns></returns>
        public static async XFTask<UI> CreateAsync<P1>(XObject source, string uiType, P1 p1, Transform parentObj)
        {
            var tag = source.TagId;
            UI ui = await Common.Instance.Get<UIManager>()?.CreateAsync(uiType, p1, parentObj);
            if (tag != source.TagId)    // 异步方法返回后会继续执行后面的逻辑，但是可能在返回前此类就释放掉了，为了避免这种情况，要用tag判断是否被释放过
            {
                ui?.Dispose();
                return null;
            }
            return ui;
        }

        /// <summary>
        /// 创建一个受UIManager管理的UI，通常是作为独立UI
        /// </summary>
        /// <param name="uiType"></param>
        /// <param name="layer"></param>
        /// <returns></returns>
        public static async XFTask<UI> CreateAsync(string uiType, UILayer layer)
        {
            UI ui = await Common.Instance.Get<UIManager>()?.CreateAsync(uiType, layer);
            return ui;
        }

        /// <summary>
        /// 异步创建一个受UIManager管理的UI，通常是作为独立UI，带一个初始化参数，如果需要更多的参数，可以自行扩展或者使用struct
        /// </summary>
        /// <typeparam name="P1"></typeparam>
        /// <param name="uiType"></param>
        /// <param name="p1"></param>
        /// <param name="layer"></param>
        /// <returns></returns>
        public static async XFTask<UI> CreateAsync<P1>(string uiType, P1 p1, UILayer layer)
        {
            UI ui = await Common.Instance.Get<UIManager>()?.CreateAsync(uiType, p1, layer);
            return ui;
        }

        /// <summary>
        /// 异步创建一个受UIManager管理的UI，通常是作为独立UI
        /// <para>层级为默认配置层级</para>
        /// </summary>
        /// <param name="uiType"></param>
        /// <returns></returns>
        public static async XFTask<UI> CreateAsync(string uiType)
        {
            UI ui = await Common.Instance.Get<UIManager>()?.CreateAsync(uiType);
            return ui;
        }

        /// <summary>
        /// 异步创建一个受UIManager管理的UI，通常是作为独立UI，带一个初始化参数，如果需要更多的参数，可以自行扩展或者使用struct
        /// <para>层级为默认配置层级</para>
        /// </summary>
        /// <typeparam name="P1"></typeparam>
        /// <param name="uiType"></param>
        /// <param name="p1"></param>
        /// <returns></returns>
        public static async XFTask<UI> CreateAsync<P1>(string uiType, P1 p1)
        {
            UI ui = await Common.Instance.Get<UIManager>()?.CreateAsync(uiType, p1);
            return ui;
        }

        #endregion

        #region Popup

        /// <summary>
        /// 创建弹窗
        /// </summary>
        /// <param name="uiType"></param>
        public static void CreatePopup(string uiType)
        {
            Common.Instance.Get<UIPopupManager>()?.Create(uiType);
        }

        /// <summary>
        /// 创建弹窗
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uiType"></param>
        /// <param name="arg"></param>
        public static void CreatePopup<T>(string uiType, T arg)
        {
            Common.Instance.Get<UIPopupManager>()?.Create(uiType, arg);
        }

        /// <summary>
        /// 清空弹窗
        /// </summary>
        public static void ClearPopup()
        {
            Common.Instance.Get<UIPopupManager>()?.Clear();
        }

        /// <summary>
        /// 移除当前打开的弹窗
        /// </summary>
        public static void RemoveCurPopup()
        {
            Common.Instance.Get<UIPopupManager>()?.RemoveCurrent();
        }

        #endregion

        public static void Remove(string uiType)
        {
            Common.Instance.Get<UIManager>()?.Remove(uiType);
        }

        public static void Clear()
        {
            Common.Instance.Get<UIManager>()?.Clear();
        }
    }
}
