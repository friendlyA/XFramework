﻿using System.Collections.Generic;

namespace XFramework
{
    namespace UIEventType
    {
        /// <summary>
        /// 当所有的UI弹窗关闭
        /// </summary>
        public struct OnAllPopupClosed
        {

        }
    }

    /// <summary>
    /// UI弹窗管理器，内部是一个队列，每次只会打开有一个弹窗，其余的进入队列等待当前弹窗关闭后再依次打开
    /// </summary>
    public sealed class UIPopupManager : CommonObject, IUpdate
    {
        private class UIPopupFlagComponent : UIComponent, IAwake<long>
        {
            private long id;

            public void Initialize(long uniqueId)
            {
                id = uniqueId;
            }

            protected override void OnDestroy()
            {
                var uniqueId = id;
                id = 0;
                Common.Instance.Get<UIPopupManager>().RemoveCurrent(uniqueId);

                base.OnDestroy();
            }
        }

        private interface IPopup : System.IDisposable
        {
            long Id { get; }

            string Key { get; }

            void Awake(UI ui);
        }

        private abstract class BasePopupData : PoolObject, IPopup
        {
            public abstract long Id { get; }

            public abstract string Key { get; }

            public abstract void Awake(UI ui);
        }

        private class PopupData : BasePopupData, IAwake<string>
        {
            private string key;

            private long id;

            public override string Key => key;

            public override long Id => id;

            public static PopupData Create(string key)
            {
                return Create<PopupData, string>(key);
            }

            public void Initialize(string args)
            {
                this.key = args;
                id = RandomHelper.GenerateInstanceId();
            }

            public override void Awake(UI ui)
            {
                ObjectHelper.Awake(ui);
            }

            protected override void OnDestroy()
            {
                key = null;
                id = 0;
            }
        }

        private class PopupData<T> : BasePopupData, IAwake<string, T>
        {
            private string key;

            private T arg;

            private long id;

            public override string Key => key;

            public override long Id => id;

            public static PopupData<T> Create(string key, T arg)
            {
                return Create<PopupData<T>, string, T>(key, arg);
            }

            public void Initialize(string key, T arg)
            {
                this.key = key;
                this.arg = arg;
                id = RandomHelper.GenerateInstanceId();
            }

            public override void Awake(UI ui)
            {
                ObjectHelper.Awake(ui, arg);
            }

            protected override void OnDestroy()
            {
                key = null;
                arg = default;
                id = 0;
            }
        }

        private Queue<IPopup> popupDatas = new Queue<IPopup>();

        /// <summary>
        /// 准备打开弹窗
        /// </summary>
        private bool IsReady;

        private long CurId => popupDatas.Count == 0 ? 0 : popupDatas.Peek().Id;

        public long Count => popupDatas.Count;

        protected override void Destroy()
        {
            base.Destroy();
            Clear();
        }

        void IUpdate.Update()
        {
            if (!IsReady)
                return;

            IsReady = false;
            Open();
        }

        private UIManager GetUIManager()
        {
            return Common.Instance.Get<UIManager>();
        }
        
        private void InitUI(UI ui, long id)
        {
            var comp = ObjectFactory.Create<UIPopupFlagComponent, long>(id, true);
            ui.AddUIComponent(comp);
        }

        private void Enqueue(string uiType)
        {
            popupDatas.Enqueue(PopupData.Create(uiType));
            if (popupDatas.Count == 1)
                Open();
        }

        private void Enqueue<T>(string uiType, T arg)
        {
            popupDatas.Enqueue(PopupData<T>.Create(uiType, arg));
            if (popupDatas.Count == 1)
                Open();
        }

        private void Open()
        {
            if (popupDatas.Count == 0)
                return;

            CreateAsync(popupDatas.Peek()).Coroutine();
        }

        private async XFTask CreateAsync(IPopup data)
        {
            var curId = CurId;
            var uiManager = GetUIManager();
            var uiMgrTagId = uiManager.TagId;
            var tagId = this.TagId;
            var ui = await uiManager.CreateAsync(data.Key, false);
            if (tagId != this.TagId || CurId == 0 || CurId != curId)
            {
                ui?.Dispose();
                return;
            }
                
            if (ui == null)
            {
                // 继续创建新的弹窗
                if (uiMgrTagId == uiManager.TagId)
                {
                    Open();
                }
                return;
            }

            InitUI(ui, data.Id);
            data.Awake(ui);
        }

        private void RemoveCurrent(long id)
        {
            if (id == CurId)
            {
                RemoveCurrent();
            }
        }

        public void Create(string uiType)
        {
            Enqueue(uiType);
        }

        public void Create<T>(string uiType, T arg)
        {
            Enqueue(uiType, arg);
        }

        public void Clear()
        {
            if (popupDatas.Count > 0)
                GetUIManager()?.Remove(popupDatas.Peek().Key);

            popupDatas.Clear();
        }

        public void RemoveCurrent()
        {
            if (popupDatas.Count == 0)
                return;

            popupDatas.Dequeue().Dispose();
            if (popupDatas.Count == 0)
            {
                EventManager.Instance.Publish(new UIEventType.OnAllPopupClosed());
                return;
            }
            IsReady = true;
        }
    }
}
