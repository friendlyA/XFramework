﻿using UnityEngine.Events;

namespace XFramework
{
    public static class UnityEventExtensions
    {
        public static void Set(this UnityEvent self, UnityAction action)
        {
            self.Clear();
            if (action != null)
                self.Add(action);
        }

        public static void Set<T>(this UnityEvent<T> self, UnityAction<T> action)
        {
            self.Clear();
            if (action != null)
                self.Add(action);
        }

        public static void Set<T1, T2>(this UnityEvent<T1, T2> self, UnityAction<T1, T2> action)
        {
            self.Clear();
            if (action != null)
                self.Add(action);
        }

        public static void Set<T1, T2, T3>(this UnityEvent<T1, T2, T3> self, UnityAction<T1, T2, T3> action)
        {
            self.Clear();
            if (action != null)
                self.Add(action);
        }

        public static void Set<T1, T2, T3, T4>(this UnityEvent<T1, T2, T3, T4> self, UnityAction<T1, T2, T3, T4> action)
        {
            self.Clear();
            if (action != null)
                self.Add(action);
        }

        public static void Add(this UnityEvent self, UnityAction action)
        {
            self.AddListener(action);
        }

        public static void Add<T>(this UnityEvent<T> self, UnityAction<T> action)
        {
            self.AddListener(action);
        }

        public static void Add<T1, T2>(this UnityEvent<T1, T2> self, UnityAction<T1, T2> action)
        {
            self.AddListener(action);
        }

        public static void Add<T1, T2, T3>(this UnityEvent<T1, T2, T3> self, UnityAction<T1, T2, T3> action)
        {
            self.AddListener(action);
        }

        public static void Add<T1, T2, T3, T4>(this UnityEvent<T1, T2, T3, T4> self, UnityAction<T1, T2, T3, T4> action)
        {
            self.AddListener(action);
        }

        public static void Remove(this UnityEvent self, UnityAction action)
        {
            self.RemoveListener(action);
        }

        public static void Remove<T>(this UnityEvent<T> self, UnityAction<T> action)
        {
            self.RemoveListener(action);
        }

        public static void Remove<T1, T2>(this UnityEvent<T1, T2> self, UnityAction<T1, T2> action)
        {
            self.RemoveListener(action);
        }

        public static void Remove<T1, T2, T3>(this UnityEvent<T1, T2, T3> self, UnityAction<T1, T2, T3> action)
        {
            self.RemoveListener(action);
        }

        public static void Remove<T1, T2, T3, T4>(this UnityEvent<T1, T2, T3, T4> self, UnityAction<T1, T2, T3, T4> action)
        {
            self.RemoveListener(action);
        }

        public static void Clear(this UnityEventBase self)
        {
            self.RemoveAllListeners();
        }
    }
}
