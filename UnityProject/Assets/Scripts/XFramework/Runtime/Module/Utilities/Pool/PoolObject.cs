﻿namespace XFramework
{
    public abstract class PoolObject : System.IDisposable
    {
        public bool IsFromPool { get; protected set; }

        public bool IsDisposed { get; protected set; }

        protected PoolObject() 
        { 
            IsFromPool = false; 
        }

        protected static T CreateInner<T>() where T : PoolObject, new()
        {
            var obj = ObjectPool.Instance.Fetch<T>();
            obj.IsDisposed = false;
            obj.IsFromPool = true;
            return obj;
        }

        public static T Create<T>() where T : PoolObject, new()
        {
            var obj = CreateInner<T>();
            ObjectHelper.Awake(obj);
            return obj;
        }

        public static T Create<T, P1>(P1 p1) where T : PoolObject, new()
        {
            var obj = CreateInner<T>();
            ObjectHelper.Awake(obj, p1);
            return obj;
        }

        public static T Create<T, P1, P2>(P1 p1, P2 p2) where T : PoolObject, new()
        {
            var obj = CreateInner<T>();
            ObjectHelper.Awake(obj, p1, p2);
            return obj;
        }

        public static T Create<T, P1, P2, P3>(P1 p1, P2 p2, P3 p3) where T : PoolObject, new()
        {
            var obj = CreateInner<T>();
            ObjectHelper.Awake(obj, p1, p2, p3);
            return obj;
        }

        public static T Create<T, P1, P2, P3, P4>(P1 p1, P2 p2, P3 p3, P4 p4) where T : PoolObject, new()
        {
            var obj = CreateInner<T>();
            ObjectHelper.Awake(obj, p1, p2, p3, p4);
            return obj;
        }

        public static T Create<T, P1, P2, P3, P4, P5>(P1 p1, P2 p2, P3 p3, P4 p4, P5 p5) where T : PoolObject, new()
        {
            var obj = CreateInner<T>();
            ObjectHelper.Awake(obj, p1, p2, p3, p4, p5);
            return obj;
        }

        public void Dispose()
        {
            if (IsDisposed)
                return;

            bool isFromPool = IsFromPool;
            IsDisposed = true;
            IsFromPool = false;

            OnDestroy();

            if (isFromPool)
                ObjectPool.Instance.Recycle(this);
        }

        protected abstract void OnDestroy();
    }
}
