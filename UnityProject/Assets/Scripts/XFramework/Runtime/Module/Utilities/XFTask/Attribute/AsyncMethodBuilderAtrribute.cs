﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Runtime.CompilerServices
{
    //
    // 摘要:
    //     Indicates the type of the async method builder that should be used by a language
    //     compiler to build the attributed type when used as the return type of an async
    //     method.
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface | AttributeTargets.Delegate, Inherited = false, AllowMultiple = false)]
    public sealed class AsyncMethodBuilderAttribute : Attribute
    {
        //
        // 摘要:
        //     Gets the type of the associated builder.
        //
        // 返回结果:
        //     The type of the associated builder.
        public Type BuilderType
        {
            get;
        }

        //
        // 摘要:
        //     Initializes a new instance of the the System.Runtime.CompilerServices.AsyncMethodBuilderAttribute
        //     class.
        //
        // 参数:
        //   builderType:
        //     The type of the associated builder.
        public AsyncMethodBuilderAttribute(Type builderType)
        {
            this.BuilderType = builderType;
        }
    }
}
