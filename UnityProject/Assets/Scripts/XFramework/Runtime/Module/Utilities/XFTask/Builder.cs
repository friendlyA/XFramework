﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Security;

namespace XFramework
{
    public struct XFAsyncTaskMethodBuilder
    {
        private XFTask tcs;

        // 1.Static Create method. 编译器调用创建buidler
        [DebuggerHidden]
        public static XFAsyncTaskMethodBuilder Create()
        {
            return new XFAsyncTaskMethodBuilder { tcs = XFTask.Create(true) };
        }

        // 2.TaskLike Task property.
        [DebuggerHidden]
        public XFTask Task => tcs;

        // 3.SetException 当出现异常时编译器调用，捕获异常
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void SetException(Exception e)
        {
            tcs.SetException(e);
        }

        // 4.SetResult 当任务成功完成时编译器调用这个方法，将该任务标记为已成功完成
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void SetResult()
        {
            tcs.SetResult();
        }

        // 5.AwaitOnCompleted 在SetResult之后编译器调用OnCompleted里的Action
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void AwaitOnCompleted<TAwaiter, TStateMachine>(ref TAwaiter awaiter, ref TStateMachine stateMachine)
            where TAwaiter : INotifyCompletion where TStateMachine : IAsyncStateMachine
        {
            awaiter.OnCompleted(stateMachine.MoveNext);
        }

        // 6.AwaitUnsafeOnCompleted 同5
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        [SecuritySafeCritical]
        public void AwaitUnsafeOnCompleted<TAwaiter, TStateMachine>(ref TAwaiter awaiter, ref TStateMachine stateMachine)
    where TAwaiter : ICriticalNotifyCompletion where TStateMachine : IAsyncStateMachine
        {
            awaiter.OnCompleted(stateMachine.MoveNext);
        }

        // 7.Start 构造之后开启状态机
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void Start<TStateMachine>(ref TStateMachine stateMachine) where TStateMachine : IAsyncStateMachine
        {
            stateMachine.MoveNext();
        }

        // 8.SetStateMachine 将生成器与指定的状态机相关联
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void SetStateMachine(IAsyncStateMachine stateMachine)
        {

        }
    }

    public struct XFAsyncTaskMethodBuilder<T>
    {
        private XFTask<T> tcs;

        // 1.Static Create method. 编译器调用创建buidler
        [DebuggerHidden]
        public static XFAsyncTaskMethodBuilder<T> Create()
        {
            return new XFAsyncTaskMethodBuilder<T> { tcs = XFTask<T>.Create(true) };
        }

        // 2.TaskLike Task property.
        [DebuggerHidden]
        public XFTask<T> Task => tcs;

        // 3.SetException 当出现异常时编译器调用，捕获异常
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void SetException(Exception e)
        {
            tcs.SetException(e);
        }

        // 4.SetResult 当任务成功完成时编译器调用这个方法，将该任务标记为已成功完成
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void SetResult(T ret)
        {
            tcs.SetResult(ret);
        }

        // 5.AwaitOnCompleted 在SetResult之后编译器调用OnCompleted里的Action
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void AwaitOnCompleted<TAwaiter, TStateMachine>(ref TAwaiter awaiter, ref TStateMachine stateMachine)
            where TAwaiter : INotifyCompletion where TStateMachine : IAsyncStateMachine
        {
            awaiter.OnCompleted(stateMachine.MoveNext);
        }

        // 6.AwaitUnsafeOnCompleted 同5
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        [SecuritySafeCritical]
        public void AwaitUnsafeOnCompleted<TAwaiter, TStateMachine>(ref TAwaiter awaiter, ref TStateMachine stateMachine)
    where TAwaiter : ICriticalNotifyCompletion where TStateMachine : IAsyncStateMachine
        {
            awaiter.OnCompleted(stateMachine.MoveNext);
        }

        // 7.Start 构造之后开启状态机
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void Start<TStateMachine>(ref TStateMachine stateMachine) where TStateMachine : IAsyncStateMachine
        {
            stateMachine.MoveNext();
        }

        // 8.SetStateMachine 将生成器与指定的状态机相关联
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void SetStateMachine(IAsyncStateMachine stateMachine)
        {

        }
    }

    internal struct XFAsyncVoidMethodBuilder
    {
        // 1.Static Create method. 编译器调用创建buidler
        [DebuggerHidden]
        public static XFAsyncVoidMethodBuilder Create()
        {
            return default;
        }

        // 2.TaskLike Task property.
        [DebuggerHidden]
        public XFVoid Task => default;

        // 3.SetException 当出现异常时编译器调用，直接打印异常
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void SetException(Exception e)
        {
            // unity log error
            UnityEngine.Debug.LogError(e);
        }

        // 4.SetResult 啥也不做
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void SetResult()
        {
            
        }

        // 5.AwaitOnCompleted 在SetResult之后编译器调用OnCompleted里的Action
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void AwaitOnCompleted<TAwaiter, TStateMachine>(ref TAwaiter awaiter, ref TStateMachine stateMachine)
            where TAwaiter : INotifyCompletion where TStateMachine : IAsyncStateMachine
        {
            awaiter.OnCompleted(stateMachine.MoveNext);
        }

        // 6.AwaitUnsafeOnCompleted 同5
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        [SecuritySafeCritical]
        public void AwaitUnsafeOnCompleted<TAwaiter, TStateMachine>(ref TAwaiter awaiter, ref TStateMachine stateMachine)
    where TAwaiter : ICriticalNotifyCompletion where TStateMachine : IAsyncStateMachine
        {
            awaiter.OnCompleted(stateMachine.MoveNext);
        }

        // 7.Start 构造之后开启状态机
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void Start<TStateMachine>(ref TStateMachine stateMachine) where TStateMachine : IAsyncStateMachine
        {
            stateMachine.MoveNext();
        }

        // 8.SetStateMachine 将生成器与指定的状态机相关联
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void SetStateMachine(IAsyncStateMachine stateMachine)
        {

        }
    }
}
