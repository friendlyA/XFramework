﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.ExceptionServices;

namespace XFramework
{
    public enum AwaiterStatus : byte
    {
        /// <summary>The operation has not yet completed.</summary>
        Pending = 0,

        /// <summary>The operation completed successfully.</summary>
        Succeeded = 1,

        /// <summary>The operation completed with an error.</summary>
        Faulted = 2,
    }

    [AsyncMethodBuilder(typeof(XFAsyncTaskMethodBuilder))]
    public class XFTask : ICriticalNotifyCompletion
    {
        private readonly static Queue<XFTask> pool = new Queue<XFTask>();

        private static XFTask completedTask;

        public static XFTask CompletedTask
        {
            get
            {
                if (completedTask is null)
                    completedTask = new XFTask { status = AwaiterStatus.Succeeded };

                return completedTask;
            }
        }

        public bool IsCompleted
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [DebuggerHidden]
            get { return status != AwaiterStatus.Pending; }
        }

        private AwaiterStatus status;

        private object callback;

        private bool isFromPool;

        private XFTask()
        {

        }

        private void Recycle()
        {
            if (!isFromPool)
                return;

            status = AwaiterStatus.Pending;
            callback = null;
            pool.Enqueue(this);
            if (pool.Count > 1000)  //太多了清一下
                pool.Clear();
        }

        /// <summary>
        /// 在理解async/await和对象池原理之前请慎用XFTask的对象池
        /// <para>用对象池的XFTask在SetResult之前请先置空</para>
        /// <para>例如</para>
        /// <example>
        /// <code>
        /// class ClassTest
        /// {
        /// XFTask tcs = XFTask.Create(true); // 使用对象池创建
        /// async XFTask WaitAsync(){
        ///     await tcs;
        /// }
        /// 
        /// void SetResult(){
        ///     var tcs = this.tcs;
        ///     this.tcs = null;  //先置空
        ///     tcs.SetResult();
        /// }
        /// }
        /// </code>
        /// </example>
        /// </summary>
        /// <param name="isFromPool">是否来自对象池</param>
        /// <returns></returns>
        public static XFTask Create(bool isFromPool = false)
        {
            if (isFromPool)
            {
                if (pool.Count > 0)
                    return pool.Dequeue();
                
                return new XFTask { isFromPool = true };
            }

            return new XFTask();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        private async XFVoid VoidCoroutine()
        {
            await this;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void Coroutine()
        {
            VoidCoroutine().Coroutine();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        void INotifyCompletion.OnCompleted(Action continuation)
        {
            (this as ICriticalNotifyCompletion).UnsafeOnCompleted(continuation);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        void ICriticalNotifyCompletion.UnsafeOnCompleted(Action continuation)
        {
            if (status != AwaiterStatus.Pending)
            {
                continuation?.Invoke();
                return;
            }

            callback = continuation;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void GetResult()
        {
            switch (status)
            {
                case AwaiterStatus.Succeeded:
                    Recycle();
                    break;
                case AwaiterStatus.Faulted:
                    ExceptionDispatchInfo c = callback as ExceptionDispatchInfo;
                    callback = null;
                    c?.Throw();
                    Recycle();
                    break;
                default:
                    throw new NotSupportedException("XFTask does not allow call GetResult directly when task not completed. Please use 'await'.");
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void SetResult()
        {
            if (status != AwaiterStatus.Pending)
            {
                throw new InvalidOperationException("Task_TransitionToFinal_AlreadyCompleted");
            }

            status = AwaiterStatus.Succeeded;
            var cb = callback as Action;
            callback = null;    //先置空
            cb?.Invoke();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void SetException(Exception e)
        {
            if (status != AwaiterStatus.Pending)
            {
                throw new InvalidOperationException("Task_TransitionToFinal_AlreadyCompleted");
            }

            status = AwaiterStatus.Faulted;
            var cb = callback as Action;
            callback = ExceptionDispatchInfo.Capture(e);
            cb?.Invoke();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public XFTask GetAwaiter()
        {
            return this;
        }
    }

    [AsyncMethodBuilder(typeof(XFAsyncTaskMethodBuilder<>))]
    public class XFTask<T> : ICriticalNotifyCompletion
    {
        private readonly static Queue<XFTask<T>> pool = new Queue<XFTask<T>>();

        public bool IsCompleted
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [DebuggerHidden]
            get { return status != AwaiterStatus.Pending; }
        }

        private AwaiterStatus status;

        private object callback;

        private bool isFromPool;

        private T value;

        private XFTask()
        {

        }

        private void Recycle()
        {
            if (!isFromPool)
                return;

            status = AwaiterStatus.Pending;
            value = default;
            callback = null;
            pool.Enqueue(this);
            if (pool.Count > 1000)  //太多了清一下
                pool.Clear();
        }

        /// <summary>
        /// 在理解async/await和对象池原理之前请慎用XFTask的对象池
        /// <para>用对象池的XFTask在SetResult之前请先置空</para>
        /// <para>例如</para>
        /// <example>
        /// <code>
        /// class ClassTest
        /// {
        /// XFTask tcs = XFTask.Create(true); // 使用对象池创建
        /// async XFTask WaitAsync(){
        ///     await tcs;
        /// }
        /// 
        /// void SetResult(){
        ///     var tcs = this.tcs;
        ///     this.tcs = null;  //先置空
        ///     tcs.SetResult();
        /// }
        /// }
        /// </code>
        /// </example>
        /// </summary>
        /// <param name="isFromPool">是否来自对象池</param>
        /// <returns></returns>
        public static XFTask<T> Create(bool isFromPool = false)
        {
            if (isFromPool)
            {
                if (pool.Count > 0)
                    return pool.Dequeue();

                return new XFTask<T> { isFromPool = true };
            }

            return new XFTask<T>();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        private async XFVoid VoidCoroutine()
        {
            await this;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void Coroutine()
        {
            VoidCoroutine().Coroutine();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        void INotifyCompletion.OnCompleted(Action continuation)
        {
            (this as ICriticalNotifyCompletion).UnsafeOnCompleted(continuation);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        void ICriticalNotifyCompletion.UnsafeOnCompleted(Action continuation)
        {
            if (status != AwaiterStatus.Pending)
            {
                continuation?.Invoke();
                return;
            }

            callback = continuation;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public T GetResult()
        {
            var value = this.value;
            switch (status)
            {
                case AwaiterStatus.Succeeded:
                    var ret = value;
                    Recycle();
                    return ret;
                case AwaiterStatus.Faulted:
                    ExceptionDispatchInfo c = callback as ExceptionDispatchInfo;
                    callback = null;
                    c?.Throw();
                    Recycle();
                    return default;
                default:
                    throw new NotSupportedException("XFTask does not allow call GetResult directly when task not completed. Please use 'await'.");
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void SetResult(T ret)
        {
            if (status != AwaiterStatus.Pending)
            {
                throw new InvalidOperationException("Task_TransitionToFinal_AlreadyCompleted");
            }

            status = AwaiterStatus.Succeeded;
            this.value = ret;
            var cb = callback as Action;
            callback = null;    //先置空
            cb?.Invoke();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public void SetException(Exception e)
        {
            if (status != AwaiterStatus.Pending)
            {
                throw new InvalidOperationException("Task_TransitionToFinal_AlreadyCompleted");
            }

            status = AwaiterStatus.Faulted;
            var cb = callback as Action;
            callback = ExceptionDispatchInfo.Capture(e);
            cb?.Invoke();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        public XFTask<T> GetAwaiter()
        {
            return this;
        }
    }

    [AsyncMethodBuilder(typeof(XFAsyncVoidMethodBuilder))]
    internal struct XFVoid : ICriticalNotifyCompletion
    {
        public bool IsCompleted => true;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        void INotifyCompletion.OnCompleted(Action continuation)
        {
            
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden]
        void ICriticalNotifyCompletion.UnsafeOnCompleted(Action continuation)
        {
            
        }

        [DebuggerHidden]
        public void Coroutine()
        {

        }
    }
}
