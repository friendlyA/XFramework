﻿using System.Collections.Generic;

namespace XFramework
{
    public static class XFTaskHelper
    {
        private class CoroutineBlocker
        {
            private int count;

            private List<XFTask> tasks = new List<XFTask>();

            public CoroutineBlocker(int count)
            {
                this.count = count;
            }

            public async XFTask WaitAsync()
            {
                --count;
                if (count < 0)
                    return;

                if (count == 0)
                {
                    var list = tasks;
                    tasks = null;
                    foreach (var task in list)
                    {
                        task.SetResult();
                    }
                    return;
                }

                XFTask tcs = XFTask.Create(true);
                tasks.Add(tcs);
                await tcs;
            }
        }

        /// <summary>
        /// 等待任一任务完成
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tasks"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public static async XFTask<bool> WaitAny<T>(IList<XFTask<T>> tasks, XCancellationToken cancellationToken = null)
        {
            if (tasks == null || tasks.Count == 0)
                return false;

            var coroutineBlocker = new CoroutineBlocker(2);
            async XFVoid WaitAsync(XFTask<T> task)
            {
                await task;
                await coroutineBlocker.WaitAsync();
            }

            foreach (var task in tasks)
            {
                WaitAsync(task).Coroutine();
            }

            await coroutineBlocker.WaitAsync();

            if (cancellationToken == null)
                return true;

            return !cancellationToken.IsCancel();
        }

        /// <summary>
        /// 等待所有任务完成
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tasks"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public static async XFTask<bool> WaitAll<T>(IList<XFTask<T>> tasks, XCancellationToken cancellationToken = null)
        {
            if (tasks == null || tasks.Count == 0)
                return false;

            var coroutineBlocker = new CoroutineBlocker(tasks.Count + 1);
            async XFVoid WaitAsync(XFTask<T> task)
            {
                await task;
                await coroutineBlocker.WaitAsync();
            }

            foreach (var task in tasks)
            {
                WaitAsync(task).Coroutine();
            }

            await coroutineBlocker.WaitAsync();

            if (cancellationToken == null)
                return true;

            return !cancellationToken.IsCancel();
        }

        /// <summary>
        /// 等待任一任务完成
        /// </summary>
        /// <param name="tasks"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public static async XFTask<bool> WaitAny(IList<XFTask> tasks, XCancellationToken cancellationToken = null)
        {
            if (tasks == null || tasks.Count == 0)
                return false;

            var coroutineBlocker = new CoroutineBlocker(2);
            async XFVoid WaitAsync(XFTask task)
            {
                await task;
                await coroutineBlocker.WaitAsync();
            }

            foreach (var task in tasks)
            {
                WaitAsync(task).Coroutine();
            }

            await coroutineBlocker.WaitAsync();

            if (cancellationToken == null)
                return true;

            return !cancellationToken.IsCancel();
        }

        /// <summary>
        /// 等待所有任务完成
        /// </summary>
        /// <param name="tasks"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public static async XFTask<bool> WaitAll(IList<XFTask> tasks, XCancellationToken cancellationToken = null)
        {
            if (tasks == null || tasks.Count == 0)
                return false;

            var coroutineBlocker = new CoroutineBlocker(tasks.Count + 1);
            async XFVoid WaitAsync(XFTask task)
            {
                await task;
                await coroutineBlocker.WaitAsync();
            }

            foreach (var task in tasks)
            {
                WaitAsync(task).Coroutine();
            }

            await coroutineBlocker.WaitAsync();

            if (cancellationToken == null)
                return true;

            return !cancellationToken.IsCancel();
        }
    }
}
